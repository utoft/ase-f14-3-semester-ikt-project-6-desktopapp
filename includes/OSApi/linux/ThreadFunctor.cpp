#include <osapi/Thread.hpp>

namespace osapi
{
  void* ThreadFunctor::threadMapper(void* thread)
  {
    /* Something is missing here - Determine what! */
    //Casting input and running the thread is missing
    ThreadFunctor* tf = static_cast<ThreadFunctor*>(thread); //Cast input parameter
    tf->run(); //run the abstract virtual function
    
    tf->threadDone_.signal();
    return NULL;
  }
}