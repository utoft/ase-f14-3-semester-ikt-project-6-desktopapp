#include <unistd.h>
#include <osapi/Utility.hpp>

namespace osapi
{
  void sleep(unsigned long msec)
  {
    usleep(msec*1000); //usleep suspends execution for 1 usec. multiply by 1000 to get milisec.
  }
}