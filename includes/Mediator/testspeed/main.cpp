// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Version 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_SP_DISABLE_THREADS

#include <string>
#include <iostream>
#include <fstream>

#include <boost/chrono.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/bind.hpp>

#include <mediator/Subscriber.hpp>
#include <mediator/DirectPublisher.hpp>
#include <mediator/GraphPublisher.hpp>
#include <mediator/Something.hpp>
#include <mediator/Message.hpp>
#include <mediator/MessageIdentifier.hpp>
#include <mediator/MediatorInstance.hpp>

using namespace mediator;

using namespace boost::chrono;

std::list< boost::shared_ptr<const Subscription> > subscriptions;

template< class Clock >
class timer
{
	typename Clock::time_point start;
public:
	timer() : start( Clock::now() ) {}
	typename Clock::duration elapsed() const
	{
		return Clock::now() - start;
	}
	double seconds() const
	{
		return elapsed().count() * ((double)Clock::period::num/Clock::period::den);
	}
};

/// A sample message class
class TestMessage : public Message
{
public:
	TestMessage( const std::string& text )
: m_text( text )
{
}

	const std::string& GetText() const
	{
		return m_text;
	}

private:
	std::string m_text;
};

/// Another sample message class
class AnotherTestMessage : public Message
{

};

void MessageHandler( const MessageIdentifier& id, boost::shared_ptr<Message> message )
{
	boost::shared_ptr<TestMessage> msg = boost::static_pointer_cast<TestMessage>(message);
	std::cout << "### MessageHandler Invoked ( " << id.toString() <<  ", " << msg->GetText() << " ) ###" <<  std::endl;
	if( id == "/test/message/hello" )
	{
		std::cout << msg->GetText();
	}
}

size_t runTest(size_t subTotal, size_t levels, MessageIdentifier toFind, boost::shared_ptr<Publisher> publisher, boost::shared_ptr<Subscriber> subscriber) {
	size_t count = 0;
	//Add some subscriptions

	std::vector< std::string > texts;
	texts.push_back("some");
	texts.push_back("word");
	texts.push_back("that");
	texts.push_back("can");
	texts.push_back("be");
	texts.push_back("used");
	texts.push_back("as");
	texts.push_back("an");
	texts.push_back("id");
	texts.push_back("it");
	texts.push_back("just");
	texts.push_back("needs");
	texts.push_back("to");
	texts.push_back("unique");
	texts.push_back("jepser");

	int lastEndPos = 0;

	for(size_t i = 0; i < levels; i++) {
		for(size_t j = 0; j < subTotal; j++) {
			MessageIdentifier id;
			for(size_t k = 0; k < i; k++ ) {

				lastEndPos = (lastEndPos + 1) % texts.size();

				id.Append(texts[lastEndPos]);
			}
			
			if(id < toFind || id == toFind)
							count++;

			//std::cout << "Sub: ID: " << id.toString() << " Type: " << MakeTypeID::magic_number_for<TestMessage>() << std::endl;
			subscriptions.push_back(subscriber->Subscribe<Message>( id, &MessageHandler ));
			subscriptions.push_back(subscriber->Subscribe<TestMessage>( id, &MessageHandler ));
			subscriptions.push_back(subscriber->Subscribe<AnotherTestMessage>( id, &MessageHandler ));
		}
	}
	return count;
}

int main()
{
	std::ofstream out("cout.txt");
	std::cout.rdbuf(out.rdbuf());

	boost::shared_ptr<DirectPublisher> direct(new DirectPublisher);
	boost::shared_ptr<GraphPublisher> graph(new GraphPublisher);

	MessageIdentifier id("/some/");

	std::cout << "TestMessage: " << MakeTypeID::magic_number_for<TestMessage>() << std::endl;
	std::cout << "AnotherTestMessage: " << MakeTypeID::magic_number_for<AnotherTestMessage>() << std::endl;
	std::cout << "Message: " << MakeTypeID::magic_number_for<Message>() << std::endl;

	timer<high_resolution_clock> a1;
	size_t counta = runTest(100000, 6, id, boost::static_pointer_cast<Publisher>(direct),boost::static_pointer_cast<Subscriber>(direct));
	high_resolution_clock::duration a2 = a1.elapsed();
	SubscriptionList lista = direct->GetMatchingList( id , MakeTypeID::magic_number_for<TestMessage>() );
	high_resolution_clock::duration a3 = a1.elapsed();

	std::cout << "Making direct list: " << duration_cast<microseconds>(a2).count() << " microseconds" << std::endl;
	std::cout << "Getting direct list: " << duration_cast<microseconds>(a3 - a2).count() << " microseconds" << std::endl;
	std::cout << "Direct list total: " << duration_cast<microseconds>(a3).count() << " microseconds" << std::endl;

	timer<high_resolution_clock> b1;
	size_t countb = runTest(100000,6, id, boost::static_pointer_cast<Publisher>(graph),boost::static_pointer_cast<Subscriber>(graph));
	high_resolution_clock::duration b2 = b1.elapsed();
	SubscriptionList listb = direct->GetMatchingList( id , MakeTypeID::magic_number_for<TestMessage>());
	high_resolution_clock::duration b3 = b1.elapsed();

	std::cout << "Making graph list: " << duration_cast<microseconds>(b2).count() << " microseconds" << std::endl;
	std::cout << "Getting graph list: " << duration_cast<microseconds>(b3 - b2).count() << " microseconds" << std::endl;
	std::cout << "Graph list total: " << duration_cast<microseconds>(b3).count() << " microseconds" << std::endl;

	std::cout << "Count should: " << counta << ", actual: " << lista.size() << std::endl;
	std::cout << "Count should: " << countb << ", actual: " << listb.size() << std::endl;

	std::cout << "TestMessage: " << MakeTypeID::magic_number_for<TestMessage>() << std::endl;
	std::cout << "AnotherTestMessage: " << MakeTypeID::magic_number_for<AnotherTestMessage>() << std::endl;
	std::cout << "Message: " << MakeTypeID::magic_number_for<Message>() << std::endl;


	return 0;
};
