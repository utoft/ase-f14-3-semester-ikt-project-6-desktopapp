// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#include <mediator/DeferredPublisher.hpp>

#include <mediator/utility/ForEach.hpp>

namespace mediator
{
void DeferredPublisher::Publish( const MessageIdentifier& id, boost::shared_ptr<Message> message )
{
	m_groups.push_back( SubscriptionGroup(GetMatchingList(id), id, message) );
}

void DeferredPublisher::Deliver()
{
	// This logic must support nested calls to Deliver(),
	// but not concurrent calls.

	GroupList localGroups;
	localGroups.swap( m_groups );

	while( !localGroups.empty() )
	{
		localGroups.front().Deliver();
		localGroups.pop_front();
	}
}

DeferredPublisher::SubscriptionGroup::SubscriptionGroup( const SubscriptionList& subscriptions,
		const MessageIdentifier& id,
		boost::shared_ptr<Message> message )
: m_subscriptions( subscriptions )
, m_id( id )
, m_message( message )
{

}

DeferredPublisher::SubscriptionGroup::SubscriptionGroup( const SubscriptionGroup& other )
: m_subscriptions( other.m_subscriptions )
, m_id( other.m_id )
, m_message( other.m_message )
{

}

void DeferredPublisher::SubscriptionGroup::Deliver()
{
	boost::shared_lock< SubscriptionList > lock( m_subscriptions );

	foreach( const SubscriptionList::value_type& i, m_subscriptions )
	{
		boost::shared_ptr< const Subscription > subscription = i.lock();
		if( subscription )
		{
			subscription->GetHandler()( m_id, m_message );
		}
	}

	// As an optimization, we don't clear m_subscriptions here, since
	// *this is about to be destroyed in DeferredPolicy::Deliver()
}

} // end namespace mediator
