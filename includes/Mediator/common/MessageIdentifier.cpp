// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#include <cstring>
#include <cassert>

#include <mediator/MessageIdentifier.hpp>

namespace mediator
{

IDNameMap MessageIdentifier::idToStringMap;

MessageIdentifier::MessageIdentifier()
{

}

MessageIdentifier::MessageIdentifier( const char* stringLiteral )
{
	PamediatoreString( stringLiteral, stringLiteral + strlen(stringLiteral) );
}

MessageIdentifier::MessageIdentifier( const std::string& string )
{
	PamediatoreString( string.begin(), string.end() );
}

void MessageIdentifier::Append( const char* stringLiteral )
{
	PamediatoreString( stringLiteral, stringLiteral + strlen(stringLiteral) );
}

void MessageIdentifier::Append( const std::string& string )
{
	PamediatoreString( string.begin(), string.end() );
}

void MessageIdentifier::RemoveToken( size_t index )
{
	assert( GetTokenCount() > index );
	m_tokens.erase( m_tokens.begin() + index );
}

size_t MessageIdentifier::GetTokenCount() const
{
	return m_tokens.size();
}

Hash MessageIdentifier::GetTokenHash( size_t index ) const
{
	assert( GetTokenCount() > index );
	return m_tokens[index];
}

// This method will return true if this MessageIdentifier is
// a valid match for the passed-in "other" MessageIdentifier.
// A match occumediator when all tokens of this Message are a valid
// subset or duplicate of the tokens of the other message.
// Note what this means:
// if "Buttons/TutorialButtons/Help3.btn" is other,
// then the MessageIdentifier "Buttons/" would match
// and "Buttons/TutorialButtons/" would match 
// but "Buttons/TutorialButtons/Help2.btn" would not match
bool MessageIdentifier::Matches( const MessageIdentifier& other ) const
{
	bool matches = false;

	if ( other.m_tokens.size() >= m_tokens.size() )
	{
		matches = std::equal( m_tokens.begin(),
				m_tokens.end(),
				other.m_tokens.begin() );
	}

	return matches;
}

std::string MessageIdentifier::toString() const {
	std::string str = "";
	for(TokenVector::const_iterator it = m_tokens.begin(); it < m_tokens.end();it++) {
		str += "/" + idToString(*it);
	}
	if(str == "")
		return "/";
	else
		return str;
}

std::string MessageIdentifier::idToString(const Hash &id) {		
	return idToStringMap.left.at(id);
}

Hash MessageIdentifier::stringToID(const std::string & str) {
	typedef IDNameMap::value_type idnamePair;

	IDNameMap::right_map::iterator it = idToStringMap.right.find(str);

	if(it == idToStringMap.right.end()) { //The key is not found. We need to add it.
		idToStringMap.insert(idnamePair(nextID(),str));
	}
	//Key is found or just added. Return it

	return idToStringMap.right.at(str);
}

size_t MessageIdentifier::nextID() {
	static size_t next(0);
	return next++;
}

bool MessageIdentifier::operator<( const MessageIdentifier& rhs ) const
{
	return boost::range::lexicographical_compare( m_tokens, rhs.m_tokens );
}

bool MessageIdentifier::operator==( const MessageIdentifier& rhs ) const
		{
	return m_tokens == rhs.m_tokens;
		}

} // end namespace mediator
