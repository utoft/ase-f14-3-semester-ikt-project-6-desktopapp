// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#include <mediator/init.hpp>

#include <mediator/GraphPublisher.hpp>

#include <mediator/Subscription.hpp>
#include <mediator/SubscriptionList.hpp>
#include <mediator/Message.hpp>
#include <mediator/MessageIdentifier.hpp>
#include <mediator/MakeMessage.hpp>
#include <mediator/messages/Signal.hpp>
#include <mediator/utility/ForEach.hpp>

namespace mediator
{

void GraphPublisher::AckPublish(const MessageType type, const MessageIdentifier& id, boost::shared_ptr<Message> message )  {
	SubscriptionList subscriptions = GetMatchingList( id , type);
	
	//boost::static_pointer_cast<Derived>(b);
	//boost::is_base_of<Base, Derived>

	foreach( const SubscriptionList::value_type& i, subscriptions ) {
		boost::shared_ptr< const Subscription > subscription = i.lock();
		if( subscription ) {
			subscription->GetHandler()( id, message );
		}
	}
}

}; // end namespace mediator
