#include "mediator/MediatorInstance.hpp"

namespace mediator
{
MediatorInstance_Wrap * MediatorInstance::mediator = NULL;

MediatorInstance::MediatorInstance(boost::shared_ptr<Publisher> pub, boost::shared_ptr<Subscriber> sub) {
	if(mediator == NULL) {
		mediator = new MediatorInstance_Wrap;
		mediator->sub = sub;
		mediator->pub = pub;
	}
}

MediatorInstance::~MediatorInstance() {
	if(mediator != NULL) {
		delete mediator;
		mediator = NULL;
	}
}

boost::shared_ptr<Publisher> MediatorInstance::getPublisher()
{
	return mediator->pub;
}

boost::shared_ptr<Subscriber> MediatorInstance::getSubscriber() {
	return mediator->sub;
}

};

