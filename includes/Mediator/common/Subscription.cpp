
#include <mediator/Subscription.hpp>

namespace mediator
{
	/// Returns the message identifier this subscription is concerned with
	const MessageIdentifier& Subscription::GetMessageIdentifier() const {
	return m_id;
}

	/// Returns the handler that will be called via this subscription
	const Handler& Subscription::GetHandler() const {
	return m_handler;
}

	const MessageType& Subscription::GetMessageType() const {
	return m_type;
}

	Subscription::Subscription(const MessageType type, const MessageIdentifier& id, const Handler& handler ) : m_type(type), m_id(id), m_handler(handler) {

}

} // end namespace mediator
