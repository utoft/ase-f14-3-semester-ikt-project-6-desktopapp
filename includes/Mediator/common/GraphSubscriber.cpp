// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#include <mediator/GraphSubscriber.hpp>

namespace mediator
{

MessageIdentifier GraphNode::getID() {
	boost::shared_ptr< GraphNode > theParent = parent.lock();
	if(theParent) {
		MessageIdentifier messageID = theParent->getID();
		messageID.Append(MessageIdentifier::idToString(ID));
		return messageID;
	} else {
		MessageIdentifier messageID("/");
		return messageID;
	}
}

SubscriptionList GraphSubscriber::GetMatchingList( const MessageIdentifier& id , const MessageType type) const {
	SubscriptionList matchingList;
	//iterate GraphNode
	boost::shared_ptr< GraphNode > currNode = m_subscriptions; //Root node

	for (size_t length = 0; length <= id.GetTokenCount(); length++) {
		//Get elements with correct type

		//Add list to matching list
		GraphMemberList::const_iterator list = currNode->members.find(type);
		if(list != currNode->members.end()) {//List found
			//Copy list
			matchingList.insert(matchingList.end(),(*list).second->begin(), (*list).second->end());
		}
		//Get id of next child to find
		if(length < id.GetTokenCount()) {
			Hash childID = id.GetTokenHash(length);

			GraphChildList::const_iterator child = currNode->children.find(childID); //Search for next child
			if(child != currNode->children.end()) { //If child exists
				currNode = (*child).second;
			} else //No matching child found
				break; //Stop searching
		} else {
			break;
		}
	}

	return matchingList;
}

boost::shared_ptr< const Subscription > GraphSubscriber::AckSubscribe(const MessageType type, const MessageIdentifier& id, Handler callback ) {
	using detail::SubscriptionDeleter;

	boost::shared_ptr< const Subscription > newSubscription(
			new Subscription(type, id, callback ),
			SubscriptionDeleter() );

	AddNewSubscription( newSubscription );
	return newSubscription;
}

void GraphSubscriber::AddNewSubscription( boost::shared_ptr<const Subscription> newSubscription ) {
	//boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
	// We give the resulting iterator to the deleter, so the weak reference to the subscription
	// can be automatically removed from the subscription collection when the subscription is destroyed
	//SubscriptionList::iterator i = m_subscriptions->insert( m_subscriptions->end(), newSubscription );
	//boost::get_deleter< detail::SubscriptionDeleter >( newSubscription )->SetSubscriptionIterator( i );
	MessageIdentifier toInsert = newSubscription->GetMessageIdentifier();
	boost::shared_ptr< GraphNode > prevNode = m_subscriptions;

	for(TokenVector::const_iterator it = toInsert.m_tokens.begin(); it != toInsert.m_tokens.end(); it++) {
		Hash id = *it;
		boost::shared_ptr< GraphNode > currNode;
		if (prevNode->children.find(id) == prevNode->children.end())//if token does not exist
		{
			//Create node
			boost::shared_ptr< GraphNode > graphNode(new GraphNode);
			currNode = graphNode;
			GraphChild newNode(id,graphNode);

			//Add token to parent token
			prevNode->children.insert(newNode);
		} else
			currNode = (*prevNode->children.find(id)).second;

		//set currNode to prevNode
		prevNode = currNode;
	}
	//Insert subscription into prevNode
	MessageType type = newSubscription->GetMessageType();

	if(prevNode->members.find(type) ==  prevNode->members.end()) { //if type not used
		//Insert type node
		boost::shared_ptr<SubscriptionList> newlist(new SubscriptionList);
		prevNode->members[type] = newlist;
	}

	//Add subscription to list
	SubscriptionList::iterator i = prevNode->members[type]->insert( prevNode->members[type]->end(), newSubscription );

	//Add deleter
	boost::get_deleter< detail::SubscriptionDeleter >( newSubscription )->SetSubscriptionIterator( i );
	boost::get_deleter< detail::SubscriptionDeleter >( newSubscription )->SetSubscriptionList( prevNode->members[type] );
}

} // end namespace mediator
