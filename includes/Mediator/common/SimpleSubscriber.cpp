// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#include <mediator/SimpleSubscriber.hpp>

namespace mediator
{

SubscriptionList SimpleSubscriber::GetMatchingList( const MessageIdentifier& id , const MessageType type) const {
	SubscriptionList matchingList;
	{
//boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
		for(SubscriptionList::iterator it = m_subscriptions->begin(); it != m_subscriptions->end(); it++)
		{
			/*boost::shared_ptr< const Subscription > subscription;
			try {
				boost::shared_ptr< const Subscription > test(*it);
				subscription = test;
			}
			catch(boost::bad_weak_ptr e) {
				continue;
			}*/

			boost::shared_ptr< const Subscription > subscription = (*it).lock();

			if( subscription &&
					subscription->GetMessageIdentifier().Matches(id) && 
					subscription->GetMessageType() == type)
			{
				matchingList.push_back( *it );
			}
		}
	}

	return matchingList;
}

boost::shared_ptr< const Subscription > SimpleSubscriber::AckSubscribe(const MessageType type, const MessageIdentifier& id, Handler callback ) {
		using detail::SubscriptionDeleter;

		boost::shared_ptr< const Subscription > newSubscription(
				new Subscription(type, id, callback ),
				SubscriptionDeleter(m_subscriptions) );

		AddNewSubscription( newSubscription );
		return newSubscription;
	}

void SimpleSubscriber::AddNewSubscription( boost::shared_ptr<const Subscription> newSubscription ) {
	//boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
		// We give the resulting iterator to the deleter, so the weak reference to the subscription
		// can be automatically removed from the subscription collection when the subscription is destroyed
		SubscriptionList::iterator i = m_subscriptions->insert( m_subscriptions->end(), newSubscription );
		boost::get_deleter< detail::SubscriptionDeleter >( newSubscription )->SetSubscriptionIterator( i );
	}

} // end namespace mediator
