// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Version 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_SP_DISABLE_THREADS

#include <string>
#include <iostream>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/bind.hpp>

#include <mediator/Subscriber.hpp>
#include <mediator/GraphPublisher.hpp>
#include <mediator/Message.hpp>
#include <mediator/MessageIdentifier.hpp>
#include <mediator/MediatorInstance.hpp>

using namespace mediator;

/// A sample message class
class TestMessage : public Message
{
public:
	TestMessage( const std::string& text )
: m_text( text )
{
}

	const std::string& GetText() const
	{
		return m_text;
	}

private:
	std::string m_text;
};

/// Another sample message class
class AnotherTestMessage : public Message
{

};

void MessageHandler( const MessageIdentifier& id, boost::shared_ptr<Message> message )
{
	boost::shared_ptr<TestMessage> msg = boost::static_pointer_cast<TestMessage>(message);
	std::cout << "### MessageHandler Invoked ( " << id.toString() <<  ", " << msg->GetText() << " ) ###" <<  std::endl;
	if( id == "/test/message/hello" )
	{
		std::cout << msg->GetText();
	}
}

void AnotherMessageHandler()
{
	std::cout << " world" << std::endl;
}

int main()
{
	boost::shared_ptr<GraphPublisher> dirpub(new GraphPublisher);
	//boost::shared_ptr<Publisher> publisher = boost::static_pointer_cast<Publisher>(dirpub);
	//boost::shared_ptr<Subscriber> subscriber = boost::static_pointer_cast<Subscriber>(dirpub);
	MediatorInstance mediatorMaster(boost::static_pointer_cast<Publisher>(dirpub),boost::static_pointer_cast<Subscriber>(dirpub));

	boost::shared_ptr<Publisher> publisher = MediatorInstance::getPublisher();
	boost::shared_ptr<Subscriber> subscriber = MediatorInstance::getSubscriber();
	// This subscription will call the MessageHandler function when a TestMessage object is 
	// published with an identifier matching "/test/message/hello".  The function will receive
	// the message identifier and message object as parameters.
	boost::shared_ptr<const Subscription> subscription1 =
			subscriber->Subscribe<TestMessage>( "/test/message", &MessageHandler );

	// This subscription will call the AnotherMessageHandler function when a TestMessage object is 
	// published with an identifier matching "/test/message/world".  The function will not be
	// passed any parameters
	boost::shared_ptr<const Subscription> subscription2 =
			subscriber->Subscribe<TestMessage>( "/test/message/world", boost::bind(&AnotherMessageHandler) );

	// Will invoke the first subscription
	publisher->Publish( "/test/message/hello", boost::make_shared<TestMessage>("hello") );//OK

	// Will invoke the second subscription
	publisher->Publish( "/test/message/world", boost::make_shared<TestMessage>("doesn't matter") );//OK

	// Won't invoke the first subscription since the message type is wrong
	publisher->Publish( "/test/message/hello", boost::make_shared<AnotherTestMessage>() );

	// Will invoke the first subscription but nothing will be printed since
	// the handler checks for a specific message identifier internally
	publisher->Publish( "/test/message/", boost::make_shared<TestMessage>("doesn't matter") );

	// Won't invoke either subscription since the message identifier doesn't match anything
	publisher->Publish( "/foo/bar", boost::make_shared<TestMessage>("doesn't matter") );

	return 0;
};
