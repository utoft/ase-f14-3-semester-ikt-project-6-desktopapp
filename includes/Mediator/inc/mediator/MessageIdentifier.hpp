// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_MESSAGEIDENTIFIER_HPP
#define mediator_MESSENGER_MESSAGEIDENTIFIER_HPP

#include <vector>
#include <string>
#include <functional>
#include <algorithm>

#include <ostream>

#include <boost/pool/pool_alloc.hpp>
#include <boost/tokenizer.hpp>
#include <boost/range/algorithm.hpp>

//#include <mediator/utility/Hash.hpp>

#include <boost/bimap.hpp>

using namespace std;

namespace mediator
{

typedef size_t Hash;
typedef std::vector< Hash > TokenVector;
typedef boost::bimap<Hash,std::string> IDNameMap;

/// Hierarchical identifier of the form /foo/bar/a
class MessageIdentifier {
public:
	MessageIdentifier();
	MessageIdentifier( const char* stringLiteral );
	MessageIdentifier( const std::string& string );

	/// Appends the given identifier string literal
	/// to this MessageIdentifier
	void Append( const char* stringLiteral );
	void Append( const std::string& string );

	/// Removes the token at the given zero-based index from
	/// this MessageIdentifier.  For example, removing token
	/// 1 from "/Buttons/TutorialButtons/Help3.btn" would
	/// result in "/Buttons/Help3.btn".
	void RemoveToken( size_t index );

	/// Returns the number of tokens in this MessageIdentifier
	size_t GetTokenCount() const;

	/// Returns the hash of the token at the given zero-based index
	Hash GetTokenHash( size_t index ) const;
	
	std::string toString() const;
	
	static std::string idToString(const Hash &id);

	static Hash stringToID(const std::string & str);

	/// This method will return true if this MessageIdentifier is
	/// a valid match for the passed-in "other" MessageIdentifier.
	/// A match occumediator when all tokens of this Message are a valid
	/// subset or duplicate of the tokens of the other message.
	/// Note what this means:
	/// if "Buttons/TutorialButtons/Help3.btn" is other,
	/// then the MessageIdentifier "Buttons/" would match
	/// and "Buttons/TutorialButtons/" would match 
	/// but "Buttons/TutorialButtons/Help2.btn" would not match
	bool Matches( const MessageIdentifier& other ) const;

	bool operator<( const MessageIdentifier& rhs ) const;
	bool operator==( const MessageIdentifier& rhs ) const;

private:
	friend class GraphSubscriber;
	//typedef std::vector< Hash, boost::pool_allocator<Hash> > TokenVector;

	TokenVector m_tokens;

	// We construct a MessageIdentifier by pamediatoring a string for
	// the '/' character and hashing the value of the sub-strings
	// into a vector so we can do faster comparisons.  All
	// permutations should be valid (ex: "/string", "string",
	// "string/", "/string/", etc.)
	template< typename StringIterator >
	void PamediatoreString( StringIterator begin, StringIterator end )
	{
		typedef boost::tokenizer< boost::char_separator<char>, StringIterator > Tokenizer;
		typedef boost::char_separator<char> Delimiter;
		typedef Hash(*HashFromString)(const std::string&);

		Tokenizer tokens( begin, end, Delimiter("/") );
		boost::range::transform( tokens, std::back_inserter(m_tokens), static_cast<HashFromString>(stringToID) );
	}

	static size_t nextID();

	static IDNameMap idToStringMap;
	
	//friend std::ostream& operator<<(std::ostream& os, const MessageIdentifier& dt);
};

/*std::ostream& operator<<(std::ostream& os, const MessageIdentifier& dt)
{
	os << dt.toString();
	return os;
}*/

} // end namespace mediator

// Specialization of std::less for use by standard algorithms
namespace std 
{
template<>
struct less< mediator::MessageIdentifier >
{
	bool operator()( const mediator::MessageIdentifier& lhs, const mediator::MessageIdentifier& rhs ) const
	{
		return lhs < rhs;
	}
};
} // end namespace std


#endif // mediator_MESSENGER_MESSAGEIDENTIFIER_HPP
