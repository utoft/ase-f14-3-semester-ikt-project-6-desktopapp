// Copyright Ciaran McCreesh 2010.
// No known license
// (See http://ciaranm.wordpress.com/2010/05/24/runtime-type-checking-in-c-without-rtti/ )

#ifndef mediator_MESSENGER_SOMETHING_HPP
#define mediator_MESSENGER_SOMETHING_HPP

#include <mediator/init.hpp>

#include <memory>
#include <boost/shared_ptr.hpp>

namespace mediator
{

class MakeTypeID
{
public:
	static int next_magic_number()
	{
		static int magic(0);
		return magic++;
	}

	template <typename T_>
	static int magic_number_for()
	{
		static int result(next_magic_number());
		return result;
	}
};

/*
class Something
{
public:
	static int next_magic_number()
	{
		static int magic(0);
		return magic++;
	}

	template <typename T_>
	public static int magic_number_for()
	{
		static int result(next_magic_number());
		return result;
	}
	
	struct SomethingValueBase
	{
		int magic_number;

		SomethingValueBase(const int m) :
			magic_number(m)
		{
		}

		virtual ~SomethingValueBase()
		{
		}
	};

	template <typename T_>
	struct SomethingValue :
			SomethingValueBase
			{
				T_ value;

				SomethingValue(const T_ & v) :
					SomethingValueBase(magic_number_for<T_>()),
					value(v)
				{
				}
			};

			boost::shared_ptr<SomethingValueBase> _value;

public:
			template <typename T_>
			Something(const T_ & t) :
			_value(new SomethingValue<T_>(t))
			{
			}

			template <typename T_>
			const T_ & as() const
			{
				if (magic_number_for<T_>() != _value->magic_number)
					throw SomethingIsSomethingElse();
				return boost::static_pointer_cast<const SomethingValue<T_> >(_value)->value;
			}
};
*/

}; // end namespace mediator

#endif // mediator_MESSENGER_HANDLER_HPP
