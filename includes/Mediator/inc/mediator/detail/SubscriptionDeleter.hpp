// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_DETAIL_SUBSCRIPTIONDELETER_HPP
#define mediator_MESSENGER_DETAIL_SUBSCRIPTIONDELETER_HPP

#include <boost/shared_ptr.hpp>

#include <mediator/SubscriptionList.hpp>

namespace mediator
{
namespace detail
{

/// Used to remove a Subscription from a SubscriptionList when
/// all shared_ptmediator to the Subscription are destroyed
class SubscriptionDeleter
{
public:
	SubscriptionDeleter( boost::shared_ptr<SubscriptionList> subscriptionList )
: m_subscriptionList( subscriptionList )
, m_iter( m_subscriptionList->end() )
{
}

	SubscriptionDeleter()
	: m_subscriptionList( )
	, m_iter( )
	{
	}

	void SetSubscriptionIterator( SubscriptionList::iterator iter )
	{
		m_iter = iter;
	}

	void SetSubscriptionList( boost::shared_ptr<SubscriptionList> list )
	{
		m_subscriptionList = list;
	}

	void operator()( Subscription* ptr )
	{
		if( ptr )
		{
			if( m_iter != m_subscriptionList->end() )
			{
				m_subscriptionList->erase( m_iter );
			}

			delete ptr;
		}
	}

private:
	boost::shared_ptr<SubscriptionList> m_subscriptionList;
	SubscriptionList::iterator m_iter;
};   

} // end namespace detail
} // end namespace mediator

#endif // mediator_MESSENGER_DETAIL_SUBSCRIPTIONDELETER_HPP
