// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_DETAIL_CONCURRENTLIST_HPP
#define mediator_MESSENGER_DETAIL_CONCURRENTLIST_HPP

#include <list>

#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/locks.hpp>

namespace mediator
{
namespace detail
{

/// A list container that is safe to read and modify concurrently.
/// This container is compatible with boost::unique_lock<> and boost::shared_lock<>
template< typename T >
class ConcurrentList
{
private:
	typedef typename std::list< T > Impl;

	Impl m_impl;
	mutable boost::shared_mutex m_accessMutex;

public:
	typedef typename Impl::iterator iterator;
	typedef typename Impl::const_iterator const_iterator;
	typedef typename Impl::value_type value_type;

	ConcurrentList()
	{

	}

	ConcurrentList( const ConcurrentList<T>& other )
	{
		boost::lock( m_accessMutex, other.m_accessMutex );
		boost::shared_lock<boost::shared_mutex> otherLock( other.m_accessMutex, boost::adopt_lock );
		boost::unique_lock<boost::shared_mutex> thisLock( m_accessMutex, boost::adopt_lock );
		m_impl.insert( m_impl.begin(), other.m_impl.begin(), other.m_impl.end() );
	}

	ConcurrentList& operator=( const ConcurrentList<T>& rhs )
	{
		boost::lock( m_accessMutex, rhs.m_accessMutex );
		boost::shared_lock<boost::shared_mutex> rhsLock( rhs.m_accessMutex, boost::adopt_lock );
		boost::unique_lock<boost::shared_mutex> thisLock( m_accessMutex, boost::adopt_lock );
		m_impl.clear();
		m_impl.insert( m_impl.begin(), rhs.m_impl.begin(), rhs.m_impl.end() );

		return *this;
	}

	const_iterator begin() const
	{
		return m_impl.begin();
	}

	const_iterator end() const
	{
		return m_impl.end();
	}

	iterator begin()
	{
		return m_impl.begin();
	}

	iterator end()
	{
		return m_impl.end();
	}

	iterator insert( iterator position, value_type value )
	{
		boost::unique_lock<boost::shared_mutex> lock( m_accessMutex );
		return m_impl.insert( position, value );
	}

	template <class InputIterator>
	void insert (iterator position, InputIterator first, InputIterator last) {
		boost::unique_lock<boost::shared_mutex> lock( m_accessMutex );
		m_impl.insert( position, first, last );
	}

	void push_back( const value_type& value )
	{
		boost::unique_lock<boost::shared_mutex> lock( m_accessMutex );
		m_impl.push_back( value );
	}

	// Note that this returns a copy instead of a reference
	value_type front() const
	{
		boost::unique_lock<boost::shared_mutex> lock( m_accessMutex );
		return m_impl.front();
	}

	void pop_front()
	{
		boost::unique_lock<boost::shared_mutex> lock( m_accessMutex );
		m_impl.pop_front();
	}

	bool empty() const
	{
		boost::shared_lock<boost::shared_mutex> lock( m_accessMutex );
		return m_impl.empty();
	}

	iterator erase( iterator position )
	{
		boost::unique_lock<boost::shared_mutex> lock( m_accessMutex );
		return m_impl.erase( position );
	}

	void swap( ConcurrentList<T>& other )
	{
		if( &other != this )
		{
			boost::lock( m_accessMutex, other.m_accessMutex );
			boost::unique_lock<boost::shared_mutex> otherLock( other.m_accessMutex, boost::adopt_lock );
			boost::unique_lock<boost::shared_mutex> thisLock( m_accessMutex, boost::adopt_lock );
			m_impl.swap( other.m_impl );
		}
	}

	size_t size() const
	{
		boost::shared_lock<boost::shared_mutex> lock( m_accessMutex );
		return m_impl.size();
	}

	//////////////////////////////////////////////////////////////////////////
	// Compatibility with boost::unique_lock<> and boost::shared_lock<>
	void lock() const
	{
		m_accessMutex.lock();
	}

	void unlock() const
	{
		m_accessMutex.unlock();
	}

	void lock_shared() const
	{
		m_accessMutex.lock_shared();
	}

	void unlock_shared() const
	{
		m_accessMutex.unlock_shared();
	}
	//////////////////////////////////////////////////////////////////////////
};

} // end namespace detail
} // end namespace mediator

#endif // mediator_MESSENGER_DETAIL_CONCURRENTLIST_HPP
