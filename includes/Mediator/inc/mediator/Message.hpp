// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_MESSAGE_HPP
#define mediator_MESSENGER_MESSAGE_HPP

namespace mediator
{

    /// Base class for message types
    class Message
    {
    public:
        virtual ~Message() = 0;
    };

    inline Message::~Message()
    {
    }

} // end namespace mediator

#endif // mediator_MESSENGER_MESSAGE_HPP
