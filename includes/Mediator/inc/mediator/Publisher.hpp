// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_PUBLISHER_HPP
#define mediator_MESSENGER_PUBLISHER_HPP

#include <mediator/init.hpp>

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

#include <mediator/MessageIdentifier.hpp>
#include <mediator/Subscription.hpp>
#include <mediator/SubscriptionList.hpp>
#include <mediator/detail/SubscriptionDeleter.hpp>

namespace mediator
{
class Message;
}

namespace mediator
{

/// Abstract interface that facilitates publishing and subscribing for messages
class Publisher : boost::noncopyable
{
public:
	virtual ~Publisher() {}

	/// Publishes the given message to handlemediator subscribed for that type of
	/// message with the message identifier "/"
	template< typename MessageType >
	void Publish( boost::shared_ptr<MessageType> message ) {
		Publish( "/", message );
	}

	/// Publishes a Signal message to handlemediator subscribed for that type of
	/// message and the given message identifier
	/*virtual void Publish( const MessageIdentifier& id ) {
		Publish( id, MakeMessage<Signal>() );
	}*/

	/// Publishes the given message to handlemediator subscribed for that type of
	/// message and the given message identifier
	template< typename MessageType >
	void Publish( const MessageIdentifier& id, boost::shared_ptr<MessageType> message ) {
		AckPublish(MakeTypeID::magic_number_for<MessageType>(), id, boost::static_pointer_cast<Message>(message));
	}

protected:
	virtual void AckPublish(const MessageType type, const MessageIdentifier& id, boost::shared_ptr<Message> message ) = 0;
};

}; // end namespace mediator

#endif // mediator_MESSENGER_PUBLISHER_HPP
