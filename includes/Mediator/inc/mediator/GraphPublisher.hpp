// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_GRAPHPUBLISHER_HPP
#define mediator_MESSENGER_GRAPHPUBLISHER_HPP

#include <mediator/Publisher.hpp>
#include <mediator/GraphSubscriber.hpp>

namespace mediator
{

/// Publisher type that publishes messages immediately
class GraphPublisher : public Publisher, public GraphSubscriber
{
public:
	
	virtual void AckPublish(const MessageType type, const MessageIdentifier& id, boost::shared_ptr<Message> message );
};

}; // end namespace mediator

#endif // mediator_MESSENGER_DIRECTPUBLISHER_HPP
