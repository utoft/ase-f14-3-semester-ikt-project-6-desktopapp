// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_MESSAGES_SIGNAL_HPP
#define mediator_MESSENGER_MESSAGES_SIGNAL_HPP

#include <mediator/Message.hpp>

namespace mediator
{

    /// Signal is the simplest derivation of Message.
    /// Its only data is its existence.
    class Signal : public Message
    {
    public:
        virtual ~Signal()
        {

        }
    };

} // end namespace mediator

#endif // mediator_MESSENGER_MESSAGES_SIGNAL_HPP
