// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Version 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef RS_UTILITY_IMPORTEXPORT_HPP
#define RS_UTILITY_IMPORTEXPORT_HPP

/*
#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32) || defined(MSC_VER)
    #define REDSHIRT_IMPORT __declspec(dllimport)
    #define REDSHIRT_EXPORT __declspec(dllexport)
#else
    #define REDSHIRT_IMPORT
    #define REDSHIRT_EXPORT
#endif
*/

#endif // RS_UTILITY_IMPORTEXPORT_HPP
