// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Version 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef RS_UTILITY_FOREACH_HPP
#define RS_UTILITY_FOREACH_HPP

#include <boost/foreach.hpp>

#define foreach BOOST_FOREACH

#endif // RS_UTILITY_FOREACH_HPP