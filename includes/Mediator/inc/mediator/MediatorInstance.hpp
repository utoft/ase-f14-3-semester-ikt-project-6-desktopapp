#ifndef MESSAGE_DISTRIBUTION_SYSTEM_MI_HPP_
#define MESSAGE_DISTRIBUTION_SYSTEM_MI_HPP_

#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>

#include "mediator/Publisher.hpp"
#include "mediator/Subscriber.hpp"
#include "mediator/MessageIdentifier.hpp"

namespace mediator
{

struct MediatorInstance_Wrap {
	boost::shared_ptr<Subscriber> sub;
	boost::shared_ptr<Publisher> pub;
};

class MediatorInstance : boost::noncopyable
{
public:
	static boost::shared_ptr<Publisher> getPublisher();

	static boost::shared_ptr<Subscriber> getSubscriber();

	MediatorInstance(boost::shared_ptr<Publisher> pub, boost::shared_ptr<Subscriber> sub);

	~MediatorInstance();
private:
	static MediatorInstance_Wrap * mediator;
};

};


#endif
