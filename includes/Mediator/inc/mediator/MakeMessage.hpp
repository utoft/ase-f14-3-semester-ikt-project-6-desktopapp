// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_MAKEMESSAGE_HPP
#define mediator_MESSENGER_MAKEMESSAGE_HPP

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/pool/pool_alloc.hpp>

namespace mediator
{
    namespace detail
    {
        template< typename T >
        struct MessageAllocator
        {
            typedef boost::fast_pool_allocator<T> type;
        };

    } // end namespace detail

    /// A template function for efficiently allocating and constructing
    /// arbitrary message objects that are ready for publishing
    template< typename T >
    boost::shared_ptr<T> MakeMessage()
    {
        return boost::allocate_shared<T>( typename detail::MessageAllocator<T>::type() );
    }

    template< typename T, typename A1 >
    boost::shared_ptr<T> MakeMessage( const A1& a1 )
    {
        return boost::allocate_shared<T>( typename detail::MessageAllocator<T>::type(), a1 );
    }

    template< typename T, typename A1, typename A2 >
    boost::shared_ptr<T> MakeMessage( const A1& a1, const A2& a2 )
    {
        return boost::allocate_shared<T>( typename detail::MessageAllocator<T>::type(), a1, a2 );
    }

    template< typename T, typename A1, typename A2, typename A3 >
    boost::shared_ptr<T> MakeMessage( const A1& a1, const A2& a2, const A3& a3 )
    {
        return boost::allocate_shared<T>( typename detail::MessageAllocator<T>::type(), a1, a2, a3 );
    }

    template< typename T, typename A1, typename A2, typename A3, typename A4 >
    boost::shared_ptr<T> MakeMessage( const A1& a1, const A2& a2, const A3& a3, const A4& a4 )
    {
        return boost::allocate_shared<T>( typename detail::MessageAllocator<T>::type(), a1, a2, a3, a4 );
    }

    template< typename T, typename A1, typename A2, typename A3, typename A4, typename A5 >
    boost::shared_ptr<T> MakeMessage( const A1& a1, const A2& a2, const A3& a3, const A4& a4, const A5& a5 )
    {
        return boost::allocate_shared<T>( typename detail::MessageAllocator<T>::type(), a1, a2, a3, a4, a5 );
    }

    template< typename T, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6 >
    boost::shared_ptr<T> MakeMessage( const A1& a1, const A2& a2, const A3& a3, const A4& a4, const A5& a5, const A6& a6 )
    {
        return boost::allocate_shared<T>( typename detail::MessageAllocator<T>::type(), a1, a2, a3, a4, a5, a6 );
    }

    template< typename T, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7 >
    boost::shared_ptr<T> MakeMessage( const A1& a1, const A2& a2, const A3& a3, const A4& a4, const A5& a5, const A6& a6, const A7& a7 )
    {
        return boost::allocate_shared<T>( typename detail::MessageAllocator<T>::type(), a1, a2, a3, a4, a5, a6, a7 );
    }

    template< typename T, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7, typename A8 >
    boost::shared_ptr<T> MakeMessage( const A1& a1, const A2& a2, const A3& a3, const A4& a4, const A5& a5, const A6& a6, const A7& a7, const A8& a8 )
    {
        return boost::allocate_shared<T>( typename detail::MessageAllocator<T>::type(), a1, a2, a3, a4, a5, a6, a7, a8 );
    }

    template< typename T, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7, typename A8, typename A9 >
    boost::shared_ptr<T> MakeMessage( const A1& a1, const A2& a2, const A3& a3, const A4& a4, const A5& a5, const A6& a6, const A7& a7, const A8& a8, const A9& a9 )
    {
        return boost::allocate_shared<T>( typename detail::MessageAllocator<T>::type(), a1, a2, a3, a4, a5, a6, a7, a8, a9 );
    }

} // end namespace mediator

#endif // mediator_MESSENGER_MAKEMESSAGE_HPP
