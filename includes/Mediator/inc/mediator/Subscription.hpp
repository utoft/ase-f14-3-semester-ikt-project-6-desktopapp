// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_SUBSCRIPTION_HPP
#define mediator_MESSENGER_SUBSCRIPTION_HPP

#include <mediator/init.hpp>

#include <boost/noncopyable.hpp>

#include <mediator/MessageIdentifier.hpp>
#include <mediator/Something.hpp>
#include <mediator/Handler.hpp>

namespace mediator
{
/// Manages the lifetime of a handler's subscription
class Subscription : boost::noncopyable // non-copyable because only available via shared_ptr
{
public:
	/// Returns the message identifier this subscription is concerned with
	const MessageIdentifier& GetMessageIdentifier() const;

	/// Returns the handler that will be called via this subscription
	const Handler& GetHandler() const;

	const MessageType& GetMessageType() const;

protected:
	// Only available via shared_ptr from Subscriber::Subscribe()
	friend class GraphSubscriber;
	friend class SimpleSubscriber;
	friend class Subscriber;
	Subscription(const MessageType type, const MessageIdentifier& id, const Handler& handler );

	MessageType m_type;
	MessageIdentifier m_id;
	Handler m_handler;
};

} // end namespace mediator

#endif // mediator_MESSENGER_SUBSCRIPTION_HPP
