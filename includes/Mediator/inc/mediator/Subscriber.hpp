// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_SUBSCRIBER_HPP
#define mediator_MESSENGER_SUBSCRIBER_HPP

#include <mediator/init.hpp>

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

#include <mediator/Subscription.hpp>
#include <mediator/MessageIdentifier.hpp>

namespace mediator {

/// Provides scoped lifetime management of
/// subscriptions made via this interface
class Subscriber : boost::noncopyable
{
public:
	virtual ~Subscriber() {}
	/// Subscribe the given callback to messages with the given identifier and type
	/// using the given publisher
	template< typename MessageType >
	boost::shared_ptr< const Subscription >
	Subscribe( const MessageIdentifier& id, Handler callback ) {
		return AckSubscribe(MakeTypeID::magic_number_for<MessageType>(),id,callback);
	}

	/// Subscribe the given callback to messages with the given type and no specific
	/// message identifier using the given publisher
	template< typename MessageType >
	boost::shared_ptr< const Subscription >
	Subscribe( Handler callback ) {
		return Subscribe<MessageType>( "/", callback );
	}
	
protected: 
	virtual boost::shared_ptr< const Subscription > AckSubscribe(const MessageType type, const MessageIdentifier& id, Handler callback ) = 0;
};

}; // end namespace mediator

#endif // mediator_MESSENGER_SUBSCRIBER_HPP
