// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_SUBSCRIPTIONLIST_HPP
#define mediator_MESSENGER_SUBSCRIPTIONLIST_HPP

#include <list>

#include <boost/weak_ptr.hpp>

#include <mediator/detail/ConcurrentList.hpp>

namespace mediator
{
    class Subscription;
}

namespace mediator
{
    /// Safe to access concurrently.
    /// This type must allow insertion and deletion without invalidating iteratomediator.
    typedef detail::ConcurrentList< boost::weak_ptr<const Subscription> > SubscriptionList;

} // end namespace mediator

#endif // mediator_MESSENGER_SUBSCRIPTIONLIST_HPP
