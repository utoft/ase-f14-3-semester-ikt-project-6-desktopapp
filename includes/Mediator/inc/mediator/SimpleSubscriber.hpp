// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_SIMPLESUBSCRIBER_HPP
#define mediator_MESSENGER_SIMPLESUBSCRIBER_HPP

#include <mediator/init.hpp>

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

#include <mediator/Subscriber.hpp>
#include <mediator/Subscription.hpp>
#include <mediator/SubscriptionList.hpp>
#include <mediator/detail/SubscriptionDeleter.hpp>
#include <mediator/MessageIdentifier.hpp>

namespace mediator {

/// Provides scoped lifetime management of
/// subscriptions made via this interface
class SimpleSubscriber : public Subscriber {
public:
	SimpleSubscriber() : m_subscriptions( new SubscriptionList ) {}

	SubscriptionList GetMatchingList( const MessageIdentifier& id , const MessageType type) const ;

protected:
	boost::shared_ptr< const Subscription > AckSubscribe(const MessageType type, const MessageIdentifier& id, Handler callback );
	
	void AddNewSubscription( boost::shared_ptr<const Subscription> newSubscription );

	boost::shared_ptr< SubscriptionList > m_subscriptions; // Held by shared_ptr to safely
	// allow orphaned Subscriptions
	boost::mutex m_mutex;
};

} // end namespace mediator

#endif // mediator_MESSENGER_SUBSCRIBER_HPP
