// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_DEFERREDPUBLISHER_HPP
#define mediator_MESSENGER_DEFERREDPUBLISHER_HPP

#include <boost/shared_ptr.hpp>

#include <mediator/Publisher.hpp>
#include <mediator/SimpleSubscriber.hpp>
#include <mediator/SubscriptionList.hpp>
#include <mediator/detail/ConcurrentList.hpp>

namespace mediator
{
class Message;
class MessageIdentifier;
}

namespace mediator
{

/// Publisher type that queues up concurrently-published messages until
/// a call to DeferredPublisher::Deliver() is made.
/// It is NOT safe to call Deliver() concurrently on the same
/// DeferredPublisher instance, but nested calls are safe.
/// A typical use-case would be several threads calling Publish() and a
/// single thread calling Deliver().
class DeferredPublisher : public Publisher, public SimpleSubscriber {
public:
	void Publish( const MessageIdentifier& id, boost::shared_ptr<Message> message );

	void Deliver();

private:
	// Binds a list of subscriptions to a specific
	// MessageIdentifier/Message pair
	class SubscriptionGroup
	{
	public:
		SubscriptionGroup( const SubscriptionList& subscriptions,
				const MessageIdentifier& id,
				boost::shared_ptr<Message> message );

		SubscriptionGroup( const SubscriptionGroup& other );

		void Deliver();

	private:
		SubscriptionList m_subscriptions;
		MessageIdentifier m_id;
		boost::shared_ptr<Message> m_message;
	};

	typedef detail::ConcurrentList< SubscriptionGroup > GroupList;

	GroupList m_groups;
	osapi::Mutex mutex;
};

} // end namespace mediator

#endif // mediator_MESSENGER_DEFERREDPUBLISHER_HPP
