// Copyright Kenneth Riddile 2011.
// Distributed under the Boost Software License, Vemediatorion 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef mediator_MESSENGER_HANDLER_HPP
#define mediator_MESSENGER_HANDLER_HPP

#include <mediator/init.hpp>

#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

namespace mediator
{
    class MessageIdentifier;
    class Message;
}

namespace mediator
{

    /// Signature of a callable entity wishing to subscribe for messages
    typedef boost::function< void( const MessageIdentifier& id, boost::shared_ptr<Message> message ) > Handler;
    typedef int MessageType;

} // end namespace mediator

#endif // mediator_MESSENGER_HANDLER_HPP
