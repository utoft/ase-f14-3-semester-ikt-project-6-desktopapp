cd ./doxygen/latex
rm *.ps *.dvi *.aux *.toc *.idx *.ind *.ilg *.log *.out *.brf *.blg *.bbl refman.pdf

latexmk -pdf -silent -pdflatex="pdflatex -synctex=1 %%O %%S" refman

