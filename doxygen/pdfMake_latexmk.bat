cd ./doxygen/latex
del /s /f *.ps *.dvi *.aux *.toc *.idx *.ind *.ilg *.log *.out *.brf *.blg *.bbl refman.pdf

rem latexmk refman
latexmk -pdf -silent -pdflatex="pdflatex -synctex=1 %%O %%S" refman

exit