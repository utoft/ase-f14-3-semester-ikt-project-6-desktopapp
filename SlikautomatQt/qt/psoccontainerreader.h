#ifndef PSOCCONTAINERREADER_H
#define PSOCCONTAINERREADER_H

#include "GenericMessage.h"
#include "ServiceLoaderModes.h"

class psocContainerReader : public ServiceLoaderModes
{
    typedef pair<size_t,int> Payload;
    typedef GenericMessage< Payload > MsgType;
    std::string m_readPath;
    mediator::MessageIdentifier m_msgid;
    size_t m_conid;
    bool m_nonblock;

public:
    psocContainerReader(size_t conID, std::string readPath, mediator::MessageIdentifier msgID, bool nonBlockStart = false);
    void run(boost::shared_ptr<mediator::Publisher> pub);

private:
    void sendMessage(boost::shared_ptr<mediator::Publisher> & pub, boost::int16_t num);
};

#endif // PSOCCONTAINERREADER_H
