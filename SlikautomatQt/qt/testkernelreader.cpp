#include "testkernelreader.h"

#include "GenericMessage.h"

#include <time.h>
#include <stdlib.h>
#include <boost/thread.hpp>

/**
 * @brief TestKernelReader::TestKernelReader Generates random events for testing
 * @param id The id to publish messages under
 * @param sleep_delay The delay between each message
 * @param nonBlockStart True if an initial message should be send default is false
 * @param from minimum value default is 0
 * @param to maximum value default is 1
 */
TestKernelReader::TestKernelReader( mediator::MessageIdentifier id
                                    , boost::posix_time::microseconds sleep_delay
                                    , bool nonBlockStart
                           , unsigned int from
                           , unsigned int to)
    : ServiceLoaderModes(true), m_id(id), m_nonblock(nonBlockStart), m_delay(sleep_delay), m_from(from), m_to(to)
{}

/**
 * @brief TestKernelReader::run Start sending messages
 * @param publisher Publisher used to send messages
 */
void TestKernelReader::run(boost::shared_ptr<mediator::Publisher> publisher) {
    char value = 0;
    std::cout << "Running " << this->m_id.toString() << std::endl;
    std::cout << "TestKernelReader: NonBlock read from: "<< this->m_id.toString() << " >>" << value << "<<" << std::endl;

    //If we need to initially read a value. Then do so
    if(m_nonblock) {
        std::cout << "TestKernelReader: NonBlock read from: "<< this->m_id.toString() << " >>" << value << "<<" << std::endl;
        value = random() % (m_to - m_from);
        value += m_from;
        sendMessage(publisher, value);
    }

    for(;;) {
        boost::this_thread::sleep(m_delay);
        std::cout << "TestKernelReader: Block read from: "<< this->m_id.toString() << " >>" << value << "<<" << std::endl;
        value = std::rand() % (m_to - m_from);
        value += m_from;
        sendMessage(publisher, value);
    }
}

/**
 * @brief TestKernelReader::sendMessage Sends a message about the change
 * @param pub the publisher to use to send the information
 * @param num the number to send.
 */
void TestKernelReader::sendMessage(boost::shared_ptr<mediator::Publisher> & pub, boost::int16_t num) {
    GenericMessage<int> * msgPtr = new GenericMessage<int>(num);
    boost::shared_ptr< GenericMessage<int> > message(msgPtr);
    pub->Publish(m_id,message);
}
