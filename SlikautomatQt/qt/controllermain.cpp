#include "controllermain.h"

#include "GenericMessage.h"

#include <mediator/messages/Signal.hpp>

/**
 * @brief ControllerMain::ControllerMain Constructor
 * @param userView The view to show when servicemode is disabled
 * @param serviceView The view to show when servicemode is enabled
 * @param runFullScreen true if it should run in fullscreen
 */
ControllerMain::ControllerMain(boost::shared_ptr<Buycandie> & userView, boost::shared_ptr<Servicemode> & serviceView, bool runFullScreen)
    : m_userView(userView), m_serviceView(serviceView), m_runFullScreen(runFullScreen), m_userViewOpen(false), m_serviceViewOpen(false)
{}

/**
 * @brief ControllerMain::subscription
 * Subscribes to servicemode events
 * @param sub
 */
void ControllerMain::subscription(boost::shared_ptr<mediator::Subscriber> sub) {
    boost::shared_ptr<const mediator::Subscription> subscrip;

    //Subscripe to events for service mode Enabled.
    subscrip = sub->Subscribe< mediator::Signal >("/servicemode/enabled",boost::bind( &ControllerMain::handleServiceModeEnabled,this));
    m_subscriptions.push_back(subscrip); //save the subscription

    //subscibe to events for service mode Disabled.
    subscrip = sub->Subscribe< mediator::Signal >("/servicemode/disabled",boost::bind( &ControllerMain::handleServiceModeDisabled,this));
    m_subscriptions.push_back(subscrip); //save the subscription

    //subscibe to events for bag inserted
    subscrip = sub->Subscribe< mediator::Signal >("/bag/inserted",boost::bind( &ControllerMain::handleBagInserted,this));
    m_subscriptions.push_back(subscrip); //save the subscription

    //subscibe to events for bag removed
    subscrip = sub->Subscribe< mediator::Signal >("/bag/removed",boost::bind( &ControllerMain::handleBagRemoved,this));
    m_subscriptions.push_back(subscrip); //save the subscription

    //subscibe to events payment Total
    subscrip = sub->Subscribe< GenericMessage<int> >("/payment/total",boost::bind( &ControllerMain::handlePaymentTotal,this, _2));
    m_subscriptions.push_back(subscrip); //save the subscription

    //subscibe to events for order done
    subscrip = sub->Subscribe< mediator::Signal >("/order/done",boost::bind( &ControllerMain::handleOrderDone,this));
    m_subscriptions.push_back(subscrip); //save the subscription
}

/**
 * @brief ControllerMain::handleServiceModeEnabled
 * Will be called by mediator when the service mode has been enabled.
 */
void ControllerMain::handleServiceModeEnabled() {
    openServiceView();
    closeUserView();
}

/**
 * @brief ControllerMain::handleServiceModeDisabled
 * Will be called by mediator when the service mode has been disabled
 */
void ControllerMain::handleServiceModeDisabled() {
    openUserView();
    closeServiceView();
}

/**
 * @brief ControllerMain::handleBagInserted Bag Inserted events
 * Is called every time the bag is inserted
 */
void ControllerMain::handleBagInserted() {
    QMetaObject::invokeMethod(m_userView.get(), "bag_inserted", Qt::QueuedConnection);
}

/**
 * @brief ControllerMain::handleBagRemoved Bag Removed events
 * Is called every time the bag is removed
 */
void ControllerMain::handleBagRemoved() {
    QMetaObject::invokeMethod(m_userView.get(), "bag_not_inserted", Qt::QueuedConnection);
}

/**
 * @brief ControllerMain::handlePaymentTotal Recieves event every time a coin is inserted
 * @param message Contains the total amount. It is reset when event /order/start is issued.
 */
void ControllerMain::handlePaymentTotal(boost::shared_ptr<mediator::Message> message) {
    boost::shared_ptr< GenericMessage< int > > msg = boost::static_pointer_cast< GenericMessage< int > >(message);
    QMetaObject::invokeMethod(m_userView.get(), "payment_event", Qt::QueuedConnection, Q_ARG(int, msg->getValue()) );
}

/**
 * @brief ControllerMain::handleOrderDone Gets event when the order is done processing
 *
 * Invokes gui signal order_done when DaemonPSoCOrderManager is finished processing the order
 */
void ControllerMain::handleOrderDone() {
    QMetaObject::invokeMethod(m_userView.get(), "order_done", Qt::QueuedConnection );
}

/**
 * @brief ControllerMain::openUserView
 * Opens the user view buy candie
 */
void ControllerMain::openUserView() {
    if(!m_userViewOpen) {
        QMetaObject::invokeMethod(m_userView.get(), "show_window", Qt::QueuedConnection, Q_ARG(bool, m_runFullScreen));
        m_userViewOpen = true;
    }
}

/**
 * @brief ControllerMain::closeUserView
 * Closes the user view Buy candie
 */
void ControllerMain::closeUserView() {
    if(m_userViewOpen) {
        QMetaObject::invokeMethod(m_userView.get(), "close_window", Qt::QueuedConnection);
        m_userViewOpen = false;
    }
}

/**
 * @brief ControllerMain::openServiceView
 * Opens Service mode view
 */
void ControllerMain::openServiceView() {
    if(!m_serviceViewOpen) {
        QMetaObject::invokeMethod(m_serviceView.get(), "show_window", Qt::QueuedConnection, Q_ARG(bool, m_runFullScreen));
        m_serviceViewOpen = true;
    }
}

/**
 * @brief ControllerMain::closeServiceView
 * Closes service mode view
 */
void ControllerMain::closeServiceView() {
    if(m_serviceViewOpen) {
        QMetaObject::invokeMethod(m_serviceView.get(), "close_window", Qt::QueuedConnection);
        m_serviceViewOpen = false;
    }
}
