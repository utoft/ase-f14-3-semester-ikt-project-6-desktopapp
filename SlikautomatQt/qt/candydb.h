#ifndef CANDYDB_H
#define CANDYDB_H

#include <map>
#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

#include "Candy.h"
#include "Container.h"

class CandyDB
{
public:
    typedef std::map<size_t, boost::shared_ptr<Candy> > CandyMap;
    typedef std::vector< boost::shared_ptr<Candy> > CandyList;

    typedef std::map<size_t, boost::shared_ptr<Container> > ContainerMap;
    typedef std::vector< boost::shared_ptr<Container> > ContainerList;

    CandyDB(const std::string & containerPath, const std::string & candyPath, const std::string & pricePath);
    void save() const;
    void load();

    boost::shared_ptr<Container> getContainer(const size_t & id) const;
    std::vector< boost::shared_ptr<Container> > getContainers() const;
    std::vector< boost::shared_ptr<Container> > getContainers(size_t candyID) const;

    boost::shared_ptr<Candy> getCandy(const std::string & name) const;
    boost::shared_ptr<Candy> getCandy(const size_t & id) const;

    std::vector< boost::shared_ptr<Candy> > getCandies() const;

    std::vector< boost::shared_ptr<Candy> > getUsedCandies() const;

    double getCandyAmount(const size_t & id) const;

    void setContainerAmount(const size_t & id, const double & amount);

    double getPrice100 () const;
    void setPrice100(double setprice);

private:
    CandyMap m_candy;
    ContainerMap m_container;
    double m_price100;
    std::string m_containerPath;
    std::string m_candyPath;
    std::string m_price100Path;
};

#endif // CANDYDB_H
