#ifndef TESTAUTOMESSAGE_H
#define TESTAUTOMESSAGE_H

#include <mediator/MessageIdentifier.hpp>


#include "ServiceLoaderModes.h"
#include "GenericMessage.h"

template<typename FromType, typename ToMessageContent>
class TestAutoMessage : public ServiceLoaderModes
{
    boost::shared_ptr<mediator::Publisher> m_pub;
    std::list< boost::shared_ptr<const mediator::Subscription> > m_subscriptions;

    mediator::MessageIdentifier m_from;
    mediator::MessageIdentifier m_to;
    ToMessageContent m_reply;

public:
    TestAutoMessage(mediator::MessageIdentifier fromID, mediator::MessageIdentifier toID, ToMessageContent replyContent)
        : m_from(fromID), m_to(toID), m_reply(replyContent) {
    }

    void subscription(boost::shared_ptr<mediator::Subscriber> sub) {
        m_subscriptions.push_back(sub->Subscribe< FromType >(m_from, boost::bind( &TestAutoMessage::handleMessage, this)));
    }

    void run(boost::shared_ptr<mediator::Publisher> pub) {
        m_pub = pub;
    }

    void handleMessage() {
        boost::shared_ptr< GenericMessage< ToMessageContent > > msgToReturn(new GenericMessage< ToMessageContent >(m_reply));
        m_pub->Publish(m_to,msgToReturn);
    }
};

#include "GenericMessage.h"


#endif // TESTAUTOMESSAGE_H
