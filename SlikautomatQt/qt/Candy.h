#ifndef CANDY_H
#define CANDY_H

#include <string>
#include <ostream>

class Candy{
public:
    Candy(size_t id, std::string name);
    std::string getCandyName() const;
    size_t getID() const;
private:
  std::string m_name;
  size_t m_id;
};

#endif // CANDY_H
