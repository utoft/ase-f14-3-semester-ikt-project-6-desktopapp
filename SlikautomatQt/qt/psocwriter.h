#ifndef PSOCWRITER_H
#define PSOCWRITER_H

#include "ServiceLoaderModes.h"

class PSoCWriter : public ServiceLoaderModes
{
    boost::shared_ptr<const mediator::Subscription> m_subscription;
    std::string m_writePath;
    mediator::MessageIdentifier m_msgid;

public:
    PSoCWriter(const std::string & writePath, const mediator::MessageIdentifier & msgid);

    void subscription(boost::shared_ptr<mediator::Subscriber> sub);

    void handleSendAmount(boost::shared_ptr<mediator::Message> message);
};

#endif // PSOCWRITER_H
