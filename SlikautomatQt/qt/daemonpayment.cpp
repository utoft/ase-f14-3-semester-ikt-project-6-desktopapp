#include "daemonpayment.h"

#include "GenericMessage.h"

DaemonPayment::DaemonPayment()
    : m_insertedMoney(0)
{
}

void DaemonPayment::subscription(boost::shared_ptr<mediator::Subscriber> sub) {
    m_subscriptions.push_back(sub->Subscribe< GenericMessage< int > >("/psoc/payment/added", boost::bind( &DaemonPayment::handleAmountAdded, this, _2)));
    m_subscriptions.push_back(sub->Subscribe< GenericMessage< int > >("/order/start", boost::bind( &DaemonPayment::handleOrderStarted, this)));
}

void DaemonPayment::run(boost::shared_ptr<mediator::Publisher> pub) {
    m_pub = pub;

    boost::shared_ptr<GenericMessage<int>  > msgAdded( new GenericMessage<int>(0) );
    m_pub->Publish("/payment/added",msgAdded);

    boost::shared_ptr<GenericMessage<int>  > msgTotal( new GenericMessage<int>(0) );
    m_pub->Publish("/payment/total",msgTotal);
}

void DaemonPayment::handleAmountAdded(boost::shared_ptr<mediator::Message> message) {
    boost::shared_ptr< GenericMessage< int > > msg = boost::static_pointer_cast< GenericMessage< int > >(message);

    int recieved = msg->getValue();

    if((recieved % 10) != 0)
        recieved = 10;
    if(recieved > 100)
        recieved = 10;
    m_insertedMoney += recieved;

    sendAmount(msg->getValue());
}


void DaemonPayment::handleOrderStarted() {
    m_insertedMoney = 0;

    sendAmount(0);
}

void DaemonPayment::sendAmount(int added) {
    boost::shared_ptr<GenericMessage<int>  > msgAdded( new GenericMessage<int>(added) );
    m_pub->Publish("/payment/added",msgAdded);

    boost::shared_ptr<GenericMessage<int>  > msgTotal( new GenericMessage<int>(m_insertedMoney) );
    m_pub->Publish("/payment/total",msgTotal);
}
