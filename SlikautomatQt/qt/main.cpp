#define OS_LINUX
#define DEVKIT8000

//Boost
#include <boost/thread.hpp>

//QT
#include <QDebug>
#include <QtGui/QApplication>

//STL
#include <iostream>
#include <fstream>

//Mediator
#include <mediator/DirectPublisher.hpp>
#include <mediator/MediatorInstance.hpp>
#include <mediator/messages/Signal.hpp>

//Local
#include "config.h"

//Local GUI
#include "buycandie.h"

//Local GUI - Support
#include "controllermain.h"
#include "candydb.h"
#include "ordermanager.h"
#include "motormanager.h"

#include "serviceloader.h"
#include "GenericMessage.h"

//Local PSOC Interface
#include "psocinit.h"

//Local Tests
#include "testmessagedumper.h"

//Local Daemons
#include "daemonservicemode.h"
#include "daemonamountchanged.h"
#include "daemonbaginserted.h"
#include "daemonpsocordermanager.h"
#include "daemonpayment.h"
#include "daemonmanualmotor.h"

using namespace mediator;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    //Redirect cout to file
    std::ofstream outF("cout.txt");
    std::cout.rdbuf(outF.rdbuf()); //redirect std::cout to out.txt!

    std::ofstream err("cerr.txt");
    std::cerr.rdbuf(err.rdbuf());

#ifdef DEVKIT8000
    std::cout << "DEVKIT8000 set" << std::endl;
#else
    std::cout << "DEVKIT8000 not set" << std::endl;
#endif

    //### Prepare Mediator ###
    boost::shared_ptr<DirectPublisher> dirpub(new DirectPublisher);
    //boost::shared_ptr<Publisher> publisher = boost::static_pointer_cast<Publisher>(dirpub);
    //boost::shared_ptr<Subscriber> subscriber = boost::static_pointer_cast<Subscriber>(dirpub);
    MediatorInstance mediatorMaster(boost::static_pointer_cast<Publisher>(dirpub),boost::static_pointer_cast<Subscriber>(dirpub));

    boost::shared_ptr<Publisher> publisher = MediatorInstance::getPublisher();
    boost::shared_ptr<Subscriber> subscriber = MediatorInstance::getSubscriber();

    //### Prepare Service loader ###
    boost::shared_ptr<ServiceLoader> serviceLoader(new ServiceLoader(publisher,subscriber));

    //CandyDB
#ifdef DEVKIT8000
    boost::shared_ptr<CandyDB> candyDB(new CandyDB("config/containerInfo.txt", "config/candyInfo.txt", "config/priceInfo.txt"));
#else
    boost::shared_ptr<CandyDB> candyDB(new CandyDB("../../config/containerInfo.txt", "../../config/candyInfo.txt", "../../config/priceInfo.txt"));
#endif
    candyDB->load();

    //Order Manager
    boost::shared_ptr<Ordermanager> orderMan(new Ordermanager);
    serviceLoader->addClass(orderMan);

    //Motor Manager
    boost::shared_ptr<MotorManager> motorManager(new MotorManager(candyDB));
    serviceLoader->addClass(motorManager);

    //Device specific includes
    PSoCInit psocInit(CONTAINER_NUM,serviceLoader);
#ifdef DEVKIT8000
    psocInit.DevKitInit();
#else
    psocInit.PCTest();
#endif

    //## Services ##

    //Bag manager
    boost::shared_ptr<DaemonBagInserted> daemonBag(new DaemonBagInserted);
    serviceLoader->addClass(daemonBag);

    //Canister amount changed
    boost::shared_ptr<DaemonAmountChanged> daemonAmountChanged(new DaemonAmountChanged(candyDB));
    serviceLoader->addClass(daemonAmountChanged);

    //Payment handler
    boost::shared_ptr<DaemonPayment> daemonPayment(new DaemonPayment);
    serviceLoader->addClass(daemonPayment);

    //Servicemode switching
    boost::shared_ptr<DaemonServiceMode> daemonServiceMode(new DaemonServiceMode(false));
    serviceLoader->addClass(daemonServiceMode);

    //Order manager
    boost::shared_ptr<DaemonPSoCOrderManager> daemonOrderManager(new DaemonPSoCOrderManager(candyDB));
    serviceLoader->addClass(daemonOrderManager);

    //Manual Motor
    boost::shared_ptr<DaemonManualMotor> daemonManualMotor(new DaemonManualMotor(500));
    serviceLoader->addClass(daemonManualMotor);

    //Test - dump messages
    boost::shared_ptr<TestMessageDumper> messageDumper(new TestMessageDumper(subscriber,"/","All Messages"));
    messageDumper->addType< mediator::Message >();
    messageDumper->addType< mediator::Signal >();
    messageDumper->addType< GenericMessage<int> >();
    messageDumper->addType< GenericMessage< pair<size_t,int> > >();
    messageDumper->addType< GenericMessage< pair<size_t,double> > >();
    messageDumper->addType< GenericMessage< map< size_t, double > > >();

    serviceLoader->addClass(messageDumper);

    //## Main Controller ##
    //QDialog * userView = new Buycandie;
    boost::shared_ptr<Buycandie> userView(new Buycandie(orderMan,candyDB));
    boost::shared_ptr<Servicemode> serviceView(new Servicemode(candyDB, motorManager));


    bool runFullScreen = false;

#ifdef DEVKIT8000 //Make it run in fullscreen
    runFullScreen = true;
#endif

    ControllerMain * mainController = new ControllerMain(userView,serviceView,runFullScreen);

    boost::shared_ptr<ControllerMain> mainView(mainController);
    serviceLoader->addClass(mainView);

    serviceLoader->exec();

    std::cout << "Init slikautomat done" << std::endl;

    return app.exec();
}
