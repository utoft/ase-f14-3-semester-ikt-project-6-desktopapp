#include "daemonbaginserted.h"

#include "GenericMessage.h"
#include <mediator/messages/Signal.hpp>

DaemonBagInserted::DaemonBagInserted()
{
}

void DaemonBagInserted::subscription(boost::shared_ptr<mediator::Subscriber> sub) {
    m_subscriptions.push_back(sub->Subscribe< GenericMessage< int > >("/psoc/bag_present/changed", boost::bind( &DaemonBagInserted::handleBagChanged, this, _2)));
}

void DaemonBagInserted::run(boost::shared_ptr<mediator::Publisher> pub) {
    m_pub = pub;
}

void DaemonBagInserted::handleBagChanged(boost::shared_ptr<mediator::Message> message) {
    boost::shared_ptr< GenericMessage< int > > msg = boost::static_pointer_cast< GenericMessage< int> >(message);
    int bagStatus = msg->getValue();

    //Forward message
    boost::shared_ptr<mediator::Signal> msgToReturn(new mediator::Signal);
    if(bagStatus == 0)
        m_pub->Publish("/bag/removed",msgToReturn);
    else
        m_pub->Publish("/bag/inserted",msgToReturn);
}
