#ifndef DAEMONAMOUNTCHANGED_H
#define DAEMONAMOUNTCHANGED_H

#include "candydb.h"
#include "ServiceLoaderModes.h"

class DaemonAmountChanged : public ServiceLoaderModes
{
    boost::shared_ptr<CandyDB> m_db;
    boost::shared_ptr<mediator::Publisher> m_pub;
    boost::shared_ptr<const mediator::Subscription> subscrip;
public:
    DaemonAmountChanged(boost::shared_ptr<CandyDB> db);

    void subscription(boost::shared_ptr<mediator::Subscriber> sub);

    void msgHandler(const mediator::MessageIdentifier& id, boost::shared_ptr<mediator::Message> message);
};

#endif // DAEMONAMOUNTCHANGED_H
