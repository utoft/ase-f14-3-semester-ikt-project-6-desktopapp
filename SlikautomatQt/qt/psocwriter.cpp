#include "psocwriter.h"

#include "GenericMessage.h"
#include <fstream>

PSoCWriter::PSoCWriter(const std::string & writePath, const mediator::MessageIdentifier & msgid)
    : m_writePath(writePath), m_msgid(msgid)
{}

void PSoCWriter::subscription(boost::shared_ptr<mediator::Subscriber> sub) {
    m_subscription = sub->Subscribe< GenericMessage< int > >(m_msgid, boost::bind( &PSoCWriter::handleSendAmount,this,_2) );
}

void PSoCWriter::handleSendAmount(boost::shared_ptr<mediator::Message> message) {
    boost::shared_ptr< GenericMessage< int > > msg = boost::static_pointer_cast< GenericMessage< int > >(message);

    //Write amount to file
    std::ofstream ofile(m_writePath.c_str());
    ofile << msg->getValue() << std::endl;
    ofile.close();
}
