#ifndef DAEMONPAYMENT_H
#define DAEMONPAYMENT_H

#include "ServiceLoaderModes.h"

class DaemonPayment : public ServiceLoaderModes
{
    int m_insertedMoney;
    boost::shared_ptr<mediator::Publisher> m_pub;
    std::list< boost::shared_ptr<const mediator::Subscription> > m_subscriptions;
public:
    DaemonPayment();

    void subscription(boost::shared_ptr<mediator::Subscriber> sub);

    void run(boost::shared_ptr<mediator::Publisher> pub);

    void handleAmountAdded(boost::shared_ptr<mediator::Message> message);
    void handleOrderStarted();

    void sendAmount(int added);
};

#endif // DAEMONPAYMENT_H
