#ifndef PSOCREADER_H
#define PSOCREADER_H

//STL
#include <string>

//Mediator
#include <mediator/MessageIdentifier.hpp>
#include <mediator/Publisher.hpp>

//Local
#include "ServiceLoaderModes.h"

class PSoCReader : public ServiceLoaderModes
{
public:
    PSoCReader(std::string readPath, mediator::MessageIdentifier id, bool nonBlockStart = false);
    void run(boost::shared_ptr<mediator::Publisher> publisher);

private:
    void sendMessage(boost::shared_ptr<mediator::Publisher> & pub, boost::int16_t num);

    std::string m_readPath;
    mediator::MessageIdentifier m_id;
    bool m_nonblock;
};

#endif // PSOCREADER_H
