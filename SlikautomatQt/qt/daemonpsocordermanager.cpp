#include "daemonpsocordermanager.h"
#include "GenericMessage.h"

#include <mediator/messages/Signal.hpp>

/**
 * @brief DaemonPSoCOrderManager::DaemonPSoCOrderManager Manages feeding of candy from containers
 * @param db Database with Container and candy information
 */
DaemonPSoCOrderManager::DaemonPSoCOrderManager(boost::shared_ptr<CandyDB> db)
    : m_db(db), m_weightLevel(0)
{}

/**
 * @brief DaemonPSoCOrderManager::subscription Subscripe to events from psoc and OrderManager
 * @param sub Subscriber class
 */
void DaemonPSoCOrderManager::subscription(boost::shared_ptr<mediator::Subscriber> sub) {
    m_subscriptions.push_back(sub->Subscribe< GenericMessage< int > >("/psoc/order_done/changed", boost::bind( &DaemonPSoCOrderManager::handleDone, this, _2)));
    m_subscriptions.push_back(sub->Subscribe< GenericMessage< map< size_t, double > > >("/order/start", boost::bind( &DaemonPSoCOrderManager::handleStart, this, _2)));
    m_subscriptions.push_back(sub->Subscribe< GenericMessage< int > >("/psoc/weight/changed", boost::bind( &DaemonPSoCOrderManager::handleWeight, this, _2)));
}

/**
 * @brief DaemonPSoCOrderManager::run Get the publisher we need to publish messages
 * @param pub
 */
void DaemonPSoCOrderManager::run(boost::shared_ptr<mediator::Publisher> pub) {
    m_pub = pub;
}

/**
 * @brief DaemonPSoCOrderManager::handleStart Recives messages when a order is to be feeded from canisters
 * @param message Message containing map of work to do
 */
void DaemonPSoCOrderManager::handleStart(boost::shared_ptr<mediator::Message> message) {
    boost::shared_ptr< GenericMessage< map< size_t, double > > > msg = boost::static_pointer_cast< GenericMessage< map< size_t, double > > >(message);
    OrderMap list = msg->getValue();

    for (OrderMap::const_iterator it = list.begin(); it != list.end(); it++) {
        size_t candyID = (*it).first;
        int amountMissing = (*it).second;

        if(m_workQueue.find(candyID) == m_workQueue.end()) {
            m_workQueue[candyID] = amountMissing;
        } else {
            m_workQueue[candyID] += amountMissing;
        }
    }

    //Reset system
    m_expectedWeightLevel = 0;
    m_containerIgnore.erase(m_containerIgnore.begin(),m_containerIgnore.end());

    //Execute
    processNextOrder();
}

/**
 * @brief DaemonPSoCOrderManager::handleDone Recieves events every time the feeding from 1 container is done
 * @param message Message containg information about the feeding success
 */
void DaemonPSoCOrderManager::handleDone(boost::shared_ptr<mediator::Message> message) {
    boost::shared_ptr< GenericMessage< int > > msg = boost::static_pointer_cast< GenericMessage< int > >(message);
    const int errorValue = 0xffff;
    int returnValue = msg->getValue();

    //Add amount to expected level on weight
    if(!m_workQueue.empty())
        m_expectedWeightLevel += (*m_workQueue.begin()).second;

    //Handle if returnValue is an error value
    if(returnValue == errorValue) { //If error
        //Get missing amount
        int missing = m_expectedWeightLevel - m_weightLevel;
        //set missing amount on current workQueue item
        m_workQueue.begin()->second = missing;
        //Add the last container to ignore list so we dont try to feed from it again
        m_containerIgnore[m_currentContainer] = true;
    } else if(!m_workQueue.empty()) { // Add expected amount from work queue
        //Remove order
        m_workQueue.erase(m_workQueue.begin()); //Remove first element in map
    }

    processNextOrder();
}

/**
 * @brief DaemonPSoCOrderManager::processNextOrder Starts the processing on the next canister with a specific type.
 */
void DaemonPSoCOrderManager::processNextOrder() {
    bool isDone = true;

    //Check if queue is empty
    if(!m_workQueue.empty()) { //If not empty
        //Get container for first element, check ignore list
        boost::shared_ptr<Container> nextContainer;
        OrderMap::const_iterator currItem = m_workQueue.begin();
        CandyDB::ContainerList containers = m_db->getContainers(currItem->first);
        for(CandyDB::ContainerList::const_iterator it = containers.begin(); it != containers.end(); it++) {
            size_t contID = (*it)->getContainerID();
            if(m_containerIgnore.find(contID) == m_containerIgnore.end()) { //If true this container is valid
                nextContainer = (*it); //Save element
                m_currentContainer = contID; // Set current container
                break; //Stop for loop
            }
        }

        if(!nextContainer) { //If no container can feed this candy
            //UNDEFIND BEHAVIOR //TODO

        } else { // Else, feedable
            std::pair<size_t,double> toSend;
            toSend.first = nextContainer->getContainerID();
            toSend.second += (*currItem).second;
            toSend.second += m_expectedWeightLevel;

            //Get amount (is currItem)
            //Send request
            boost::shared_ptr< GenericMessage< pair<size_t,int> > > msg(new GenericMessage< pair<size_t,int> >(toSend));
            //Send it
            m_pub->Publish("/psoc/canister/feed",msg);
        }
    } else { //Else, jobs done.
        //Send order_done
        boost::shared_ptr<mediator::Signal> msgToReturn(new mediator::Signal);
        m_pub->Publish("/order/done",msgToReturn);
    }
}

void DaemonPSoCOrderManager::handleWeight(boost::shared_ptr<mediator::Message> message) {
    boost::shared_ptr< GenericMessage< int > > msg = boost::static_pointer_cast< GenericMessage< int > >(message);
    m_weightLevel = msg->getValue();
}
