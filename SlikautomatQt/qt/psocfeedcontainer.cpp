#include "psocfeedcontainer.h"

#include "GenericMessage.h"

#include <fstream>

/**
 * @brief PSoCFeedContainer::PSoCFeedContainer Handles sending messages about feeding containers
 * @param conPath the path to write the amount we want to feed to
 */
PSoCFeedContainer::PSoCFeedContainer(std::string conPath)
    : m_writePath(conPath)
{}

/**
 * @brief PSoCFeedContainer::subscription Subscribe to messages with feed requests
 * @param sub
 */
void PSoCFeedContainer::subscription(boost::shared_ptr<mediator::Subscriber> sub) {
    m_subscriptions.push_back(sub->Subscribe< GenericMessage< pair<size_t,int> > >("/psoc/canister/feed",boost::bind( &PSoCFeedContainer::handleSendAmount, this, _2)));
}

/**
 * @brief PSoCFeedContainer::handleSendAmount Handle requests to start feeding from a certain container
 * @param message
 */
void PSoCFeedContainer::handleSendAmount(boost::shared_ptr<mediator::Message> message) {
    boost::shared_ptr< GenericMessage< pair<size_t,int> > > msg = boost::static_pointer_cast< GenericMessage< pair<size_t,int> > >(message);
    size_t conID = msg->getValue().first;
    size_t amount = msg->getValue().second;

    //Build path with a number in it
    stringstream strBuild;
    strBuild << m_writePath;
    strBuild << conID;
    string path = strBuild.str();

    //Write amount to file
    std::ofstream ofile(path.c_str());
    ofile << amount << std::endl;
    ofile.close();
}
