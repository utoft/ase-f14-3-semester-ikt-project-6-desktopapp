#!/bin/bash
echo enable > /sys/class/cplddrv/cpld/gpio_encapsulation_state
echo 0x3 > /sys/class/cplddrv/cpld/spi_route_reg
echo 0x1 > /sys/class/cplddrv/cpld/ext_serial_if_route_reg
rmmod leds_gpio
insmod PSoCmod.ko
insmod hotplug_PSoC_spi_device1.ko
# insmod hotplug_PSoC_spi_device2.ko
