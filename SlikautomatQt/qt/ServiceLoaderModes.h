#ifndef SERVICELOADERMODES_H
#define SERVICELOADERMODES_H

#include <boost/shared_ptr.hpp>
#include <mediator/Publisher.hpp>
#include <mediator/Subscriber.hpp>

/**
 * @brief The ServiceLoaderModes struct
 *
 * Contains functions Service loader will call doing initialization.
 * The order is:
 * * Subscription
 * * run
 * * cleanup
 */
struct ServiceLoaderModes {
    /**
     * @brief ServiceLoaderModes
     * @param runThreaded true if run should be executed in a new thread
     */
    ServiceLoaderModes(bool runThreaded = false) : m_runThreaded(runThreaded) {}

    /**
     * @brief ~ServiceLoaderModes
     * Virtual destructor to allow destruction of children
     */
    virtual ~ServiceLoaderModes() {}

    /**
     * @brief subscription
     * Register all the required subscriptions here.
     * This function should allways be done before any messages is sent in the program
     * @param sub Subscriber class that can be used to subscribe to different messages.
     */
    virtual void subscription(boost::shared_ptr<mediator::Subscriber> sub) {}

    /**
     * @brief run main loop for a daemon. It can be run inside a thread
     * @param publisher Publisher to be used when publishing messages.
     */
    virtual void run(boost::shared_ptr<mediator::Publisher> publisher) {}

    /**
     * @brief cleanup
     * To be run when program is shutting down
     */
    virtual void cleanup() {}
private:
    friend class ServiceLoader;
    const bool m_runThreaded;
};

#endif // SERVICELOADERMODES_H
