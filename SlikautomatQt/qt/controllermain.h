#ifndef CONTROLLERMAIN_H
#define CONTROLLERMAIN_H

#include <list>

#include "ServiceLoaderModes.h"
#include "buycandie.h"
#include "servicemode.h"

class ControllerMain : public ServiceLoaderModes
{
    /**
     * @brief subscriptions
     * List of subscriptions.
     */
    std::list< boost::shared_ptr<const mediator::Subscription> > m_subscriptions;

    boost::shared_ptr<Buycandie> m_userView;
    boost::shared_ptr<Servicemode> m_serviceView;

    bool m_runFullScreen;
    bool m_userViewOpen;
    bool m_serviceViewOpen;
public:
    ControllerMain(boost::shared_ptr<Buycandie> & userView, boost::shared_ptr<Servicemode> & serviceView, bool runFullScreen = false);

    void subscription(boost::shared_ptr<mediator::Subscriber> sub);

    //## Incoming events ##
    void handleServiceModeEnabled();

    void handleServiceModeDisabled();

    void handleBagInserted();

    void handleBagRemoved();

    void handlePaymentTotal(boost::shared_ptr<mediator::Message> message);

    void handleOrderDone();

    //## Open/Close windows ##
    void openUserView();

    void closeUserView();

    void openServiceView();

    void closeServiceView();
};

#endif // CONTROLLERMAIN_H
