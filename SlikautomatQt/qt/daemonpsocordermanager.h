#ifndef DAEMONPSOCORDERMANAGER_H
#define DAEMONPSOCORDERMANAGER_H

#include "ServiceLoaderModes.h"
#include "candydb.h"
#include "ordermanager.h"

class DaemonPSoCOrderManager : public ServiceLoaderModes
{
    typedef map< size_t, double > OrderMap;
    typedef pair< size_t, int >  ContainerPair;
    typedef list< ContainerPair > ContainerWorkList;
    typedef map< size_t, bool > IgnoreList;

    boost::shared_ptr<CandyDB> m_db;

    boost::shared_ptr<mediator::Publisher> m_pub;
    std::list< boost::shared_ptr<const mediator::Subscription> > m_subscriptions;

    OrderMap m_workQueue;
    size_t m_currentContainer;
    IgnoreList m_containerIgnore;

    //Weight
    int m_weightLevel;
    int m_expectedWeightLevel;

public:
    DaemonPSoCOrderManager(boost::shared_ptr<CandyDB> db);

    void subscription(boost::shared_ptr<mediator::Subscriber> sub);

    void run(boost::shared_ptr<mediator::Publisher> pub);

    void handleStart(boost::shared_ptr<mediator::Message> message);

    void handleDone(boost::shared_ptr<mediator::Message> message);

    void handleWeight(boost::shared_ptr<mediator::Message> message);

    void processNextOrder();
};

#endif // DAEMONPSOCORDERMANAGER_H
