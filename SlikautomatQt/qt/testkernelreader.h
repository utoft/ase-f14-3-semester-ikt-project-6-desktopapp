#ifndef TESTKERNELREADER_H
#define TESTKERNELREADER_H

#include "ServiceLoaderModes.h"

#include <boost/chrono.hpp>

class TestKernelReader : public ServiceLoaderModes
{
public:
    TestKernelReader( mediator::MessageIdentifier id
                     , boost::posix_time::microseconds sleep_delay
                     , bool nonBlockStart = false
            , unsigned int from = 0
            , unsigned int to = 1);

    void run(boost::shared_ptr<mediator::Publisher> publisher);

private:

    void sendMessage(boost::shared_ptr<mediator::Publisher> & pub, boost::int16_t num);

    mediator::MessageIdentifier m_id;
    bool m_nonblock;

    boost::posix_time::microseconds m_delay;
    unsigned int m_from;
    unsigned int m_to;
};

#endif // TESTKERNELREADER_H
