#ifndef GENERICMESSAGE_H
#define GENERICMESSAGE_H

#include <mediator/Message.hpp>

template<typename T>
struct GenericMessage : public mediator::Message {
    GenericMessage(T value) : mvalue(value) {}
    T getValue() { return mvalue; }

private:
    T mvalue;
};

#endif // GENERICMESSAGE_H
