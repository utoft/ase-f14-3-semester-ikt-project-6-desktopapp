#include "daemonservicemode.h"
#include "GenericMessage.h"
#include <mediator/messages/Signal.hpp>

/**
 * @brief DaemonServiceMode::DaemonServiceMode
 * @param serviceModeOn Set true if system initially should be in servicemode false otherwise
 */
DaemonServiceMode::DaemonServiceMode(bool serviceModeOn)
    : serviceOn(serviceModeOn)
{}

/**
 * @brief DaemonServiceMode::subscription Subscibes to messages
 * @param sub
 */
void DaemonServiceMode::subscription(boost::shared_ptr<mediator::Subscriber> sub) {
    //sub->Subscribe<mediator::Message>(m_id,boost::bind( &TestMessageDumper::msgHandler, this, _1, _2));
    subscrip = sub->Subscribe< GenericMessage<int> >("/psoc/servicemode/changed",boost::bind( &DaemonServiceMode::handleChanged,this));
}

/**
 * @brief DaemonServiceMode::run Starts gui by either sending a service mode enabled or disabled message
 * @param publisher
 */
void DaemonServiceMode::run(boost::shared_ptr<mediator::Publisher> publisher) {
    pub = publisher;
    boost::shared_ptr< mediator::Signal > mess(new mediator::Signal);

    //send initial message
    if(serviceOn) {
        pub->Publish("/servicemode/enabled",mess);
    } else {
        pub->Publish("/servicemode/disabled",mess);
    }
}

/**
 * @brief DaemonServiceMode::handleChanged Recieve messages about when service mode have changed.
 * Contains cooldown. Servicemode can only be changed by a certain speed
 */
void DaemonServiceMode::handleChanged() {
    boost::shared_ptr< mediator::Signal > mess(new mediator::Signal);
    if(serviceOn) {
        serviceOn = false;
        pub->Publish("/servicemode/disabled",mess);
    } else {
        serviceOn = true;
        pub->Publish("/servicemode/enabled",mess);
    }
}
