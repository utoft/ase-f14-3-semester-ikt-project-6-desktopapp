#ifndef PSOCINIT_H
#define PSOCINIT_H

#include <list>

#include "serviceloader.h"

#include "psocreader.h"
#include "psocwriter.h"
#include "psoccontainerreader.h"
#include "psocfeedcontainer.h"

#include "testkernelreader.h"
#include "testautomessage.h"

class PSoCInit
{
    std::list< boost::shared_ptr<psocContainerReader> > psoc_con_amount;
    std::list< boost::shared_ptr<PSoCFeedContainer> > psoc_con_feed;
    std::list< boost::shared_ptr<PSoCWriter> > psoc_con_writer;
    boost::shared_ptr<ServiceLoader>  m_serviceLoader;
    const size_t m_containers;
    std::list< boost::shared_ptr<ServiceLoaderModes> > m_services;

public:
    PSoCInit(const size_t & containerCount, boost::shared_ptr<ServiceLoader> & serviceLoader);
    void DevKitInit();
    void PCTest();

    template<typename T>
    void save(boost::shared_ptr<T> item);
};

#endif // PSOCINIT_H
