#include "psoccontainerreader.h"

#include "GenericMessage.h"

#include <limits>

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <boost/scoped_ptr.hpp>

/**
 * @brief psocContainerReader::psocContainerReader Reads container information
 * @param conID Id of container
 * @param readPath Path to read from
 * @param msgID Id to publish messages under
 * @param nonBlockStart If we initially should read nonblocking
 */
psocContainerReader::psocContainerReader(size_t conID, std::string readPath, mediator::MessageIdentifier msgID, bool nonBlockStart)
    : ServiceLoaderModes(true), m_readPath(readPath), m_msgid(msgID), m_conid(conID), m_nonblock(nonBlockStart) {}

/**
 * @brief psocContainerReader::run Reads from the container. This needs to run in it's own thread
 * @param publisher Publisher to use to pubsish messages
 */
void psocContainerReader::run(boost::shared_ptr<mediator::Publisher> publisher) {
    int fd = 0;
    size_t length = 0;
    char value[8] = {0};

    //If we need to initially read a value. Then do so
    if(m_nonblock) {
        fd = open(m_readPath.c_str(), O_RDONLY | O_NONBLOCK);

        if(fd < 0) {
            std::cout << "PSocCreator: NonBlock: Error: "<<  fd << " File not found " << m_readPath << std::endl;
            close(fd);
            return;
        }

        length = read(fd,value,sizeof(value));

        std::cout << "PSocCreator: NonBlock read from " << m_readPath << " length: " << length << " >>" << value << "<<" << std::endl;

        if(length != 4294967295)
        sendMessage(publisher,atoi(value));

        close(fd);
    }

    //write(led,&ledvalue,sizeof(ledvalue));

    fd = open(m_readPath.c_str(), O_RDONLY);

    if(fd < 0) {
        std::cout << "PSocCreator: Block: Error: "<<  fd << " File not found " << m_readPath << std::endl;
        close(fd);
        return;
    }

    for(;;) { //TODO: Find way to stop loop
        length = read(fd,value,sizeof(value));
        std::cout << "PSocCreator: Block read from " << m_readPath << " length: " << length << " >>" << value << "<<" << std::endl;
        if(length != 4294967295)
        sendMessage(publisher,atoi(value));
    }

    close(fd);
}

/**
 * @brief psocContainerReader::sendMessage Sends message about level in container have changed
 * @param pub
 * @param num
 */
void psocContainerReader::sendMessage(boost::shared_ptr<mediator::Publisher> & pub, boost::int16_t num) {
    Payload infoPair(m_conid,num);
    MsgType * msgPtr = new MsgType(infoPair);
    boost::shared_ptr< MsgType > message(msgPtr);
    pub->Publish(m_msgid,message);
}
