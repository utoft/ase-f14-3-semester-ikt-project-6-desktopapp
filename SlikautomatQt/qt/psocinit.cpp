#include "psocinit.h"
#include <iostream>

PSoCInit::PSoCInit(const size_t & containerCount,boost::shared_ptr<ServiceLoader> & serviceLoader)
    : m_containers(containerCount), m_serviceLoader(serviceLoader)
{}

void PSoCInit::DevKitInit() {
    std::cout << "Constructing Weight read" << std::endl;
    //Weight - r
    boost::shared_ptr<PSoCReader> psocWeigth(new PSoCReader("/dev/WEIGHT","/psoc/weight/changed"));
    m_serviceLoader->addClass(psocWeigth);
    save(psocWeigth);

    std::cout << "Constructing bag read" << std::endl;
    //BagPresent - r
    boost::shared_ptr<PSoCReader> psocBagPresent(new PSoCReader("/dev/BAG_PRESENT","/psoc/bag_present/changed",true));
    m_serviceLoader->addClass(psocBagPresent);
    save(psocBagPresent);

    std::cout << "Constructing order read" << std::endl;
    //Order_Done - r
    boost::shared_ptr<PSoCReader> psocOrderDone(new PSoCReader("/dev/ORDER_DONE","/psoc/order_done/changed"));
    m_serviceLoader->addClass(psocOrderDone);
    save(psocOrderDone);

    std::cout << "Constructing payment read" << std::endl;
    //Payment - r
    boost::shared_ptr<PSoCReader> psocPayment(new PSoCReader("/dev/PAYMENT","/psoc/payment/added"));
    m_serviceLoader->addClass(psocPayment);
    save(psocPayment);

    std::cout << "Constructing servicemode read" << std::endl;
    //ServiceMode - r
    boost::shared_ptr<PSoCReader> psocServiceMode(new PSoCReader("/dev/SERVICEMODE","/psoc/servicemode/changed"));
    m_serviceLoader->addClass(psocServiceMode);
    save(psocServiceMode);

    std::cout << "Constructing Container read" << std::endl;
    //Container_x - r
    for(int i = 0; i < m_containers; i++) {
        boost::shared_ptr<psocContainerReader> psocCon(new psocContainerReader(i+1,"/dev/CONTAINER" + boost::to_string(i+1),"/psoc/canister/amount/changed/" + boost::to_string(i+1),true));
        psoc_con_amount.push_back(psocCon);
        m_serviceLoader->addClass(psocCon);
    }

    std::cout << "Constructing Container write" << std::endl;
    //Container_x - w
    boost::shared_ptr<PSoCFeedContainer> psocManMotor(new PSoCFeedContainer("/dev/CONTAINER"));
    save(psocManMotor);
    m_serviceLoader->addClass(psocManMotor);


    //Manual_Container_x - r
    /*for(int i = 0; i < CONTAINER_NUM; i++) {
        boost::shared_ptr<PSoCReader> psocManCon(new PSoCReader("/dev/MANUEL_CONTAINER" + boost::to_string(i+1),"/psoc/canister/motor/changed/" + boost::to_string(i+1)));
        psoc_containers.push_back(psocManCon);
        m_serviceLoader->addClass(psocManCon);
    }*/

    std::cout << "Constructing Manual Container read" << std::endl;
    //Manual_Container_x - w
    for(int i = 0; i < m_containers; i++) {
        boost::shared_ptr<PSoCWriter> psocCon(new PSoCWriter("/dev/MANUEL_CONTAINER" + boost::to_string(i+1), "/psoc/canister/motor/" + boost::to_string(i+1)));
        psoc_con_writer.push_back(psocCon);
        m_serviceLoader->addClass(psocCon);
    }
}

void PSoCInit::PCTest() {
    boost::posix_time::microseconds sec5(5*1000*1000);
    boost::posix_time::microseconds sec20(20*1000*1000);


    boost::shared_ptr<TestKernelReader> psocWeigth(new TestKernelReader("/psoc/weight/changed",sec5,true,0,1000));
    m_serviceLoader->addClass(psocWeigth);
    save(psocWeigth);

    std::cout << "Constructing bag read" << std::endl;
    //BagPresent - r
    boost::shared_ptr<TestKernelReader> psocBagPresent(new TestKernelReader("/psoc/bag_present/changed",sec5,true,0,1000));
    m_serviceLoader->addClass(psocBagPresent);
    save(psocBagPresent);

    std::cout << "Constructing order read" << std::endl;
    //Order_Done - r
    typedef TestAutoMessage<  GenericMessage< pair<size_t,int> >, GenericMessage< int > > TestAutoType;
    boost::shared_ptr< TestAutoType > psocOrderDone(new TestAutoType("/psoc/canister/feed", "/psoc/order_done/changed",1));
    m_serviceLoader->addClass(psocOrderDone);
    save(psocOrderDone);

    std::cout << "Constructing payment read" << std::endl;
    //Payment - r
    boost::shared_ptr<TestKernelReader> psocPayment(new TestKernelReader("/psoc/payment/added",sec5,false,1,2));
    m_serviceLoader->addClass(psocPayment);
    save(psocPayment);

    std::cout << "Constructing servicemode read" << std::endl;
    //ServiceMode - r
    //boost::shared_ptr<TestKernelReader> psocServiceMode(new TestKernelReader("/psoc/servicemode/changed",sec5));
    //m_serviceLoader->addClass(psocServiceMode);
    //save(psocServiceMode);
}

template<typename T>
void PSoCInit::save(boost::shared_ptr<T> item) {
    m_services.push_back(boost::static_pointer_cast< ServiceLoaderModes >(item));
}
