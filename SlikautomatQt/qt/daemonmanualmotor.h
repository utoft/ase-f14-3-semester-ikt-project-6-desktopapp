#ifndef DAEMONMANUALMOTOR_H
#define DAEMONMANUALMOTOR_H

#include "ServiceLoaderModes.h"

#include "GenericMessage.h"

class DaemonManualMotor : public ServiceLoaderModes
{
    typedef GenericMessage< pair<size_t,double> > MsgRecieveType;
    typedef GenericMessage< int > MsgSendType;

    boost::shared_ptr<const mediator::Subscription> m_subscription;
    boost::shared_ptr<mediator::Publisher> m_pub;
    int m_speedSteps;
public:
    DaemonManualMotor(const int & speedSteps);

    void subscription(boost::shared_ptr<mediator::Subscriber> sub);

    void run(boost::shared_ptr<mediator::Publisher> pub);

    void handleSetSpeed(boost::shared_ptr<mediator::Message> message);
};

#endif // DAEMONMANUALMOTOR_H
