#include "candydb.h"

#include <fstream>
#include <iostream>

#include <list>

#include <string>
#include <sstream>

/**
 * @brief CandyDB::CandyDB Database containing info about canisters and candy types
 * @param containerPath File path to load info about containers
 * @param candyPath File path to load info about candy
 * @param pricePath File that contains price for 100g of candy
 */
CandyDB::CandyDB(const std::string & containerPath, const std::string & candyPath, const std::string & pricePath)
    : m_containerPath(containerPath) ,m_candyPath(candyPath), m_price100Path(pricePath)
{}

/**
 * @brief CandyDB::save Save information about canisters
 */
void CandyDB::save() const {
    std::ofstream outfile;

    //Save containers
    outfile.open(m_containerPath.c_str(),std::ofstream::out | std::ofstream::trunc);

    ContainerList containers = this->getContainers();

    for(ContainerList::const_iterator it = containers.begin(); it != containers.end(); it++) {
        outfile << (*it)->getContainerID();
        outfile << " ";
        outfile << (*it)->getContents();
        outfile << " ";
        outfile << (*it)->getCandyAmount();
        outfile << " ";
        outfile << (int)((*it)->getManualSpeed() * 1000);

        outfile << std::endl;
    }

    outfile.close();


    //Write Price

    outfile.open(m_price100Path.c_str(),std::ofstream::out | std::ofstream::trunc);

    outfile << m_price100;
}

/**
 * @brief CandyDB::load Load information about candy and canisters
 */
void CandyDB::load() {
    // Read Containers

    std::ifstream infile;
    infile.open(m_containerPath.c_str());
    std::string inputText;

    while(!infile.eof())
    {
        int containerID;
        int candyID;
        int candyAmount;
        int maxSpeed;

        try {
            std::getline(infile,inputText);
            std::stringstream ss;
            ss << inputText;

            ss >> containerID;
            ss >> candyID;
            ss >> candyAmount;
            ss >> maxSpeed;

        }
        catch( ... ) {
            break;
        }

        boost::shared_ptr<Container> container(new Container(containerID));
        container->setContents(candyID);
        container->setCandyAmount(candyAmount);
        container->setManualSpeed(1.0*maxSpeed/1000);

        if(m_container.find(containerID) == m_container.end()) {
            std::cout << "Loading a container" << std::endl;
            m_container[containerID] = container;
        }
    }

    infile.close();

    // Read Candy

    infile.open(m_candyPath.c_str());

    while(!infile.eof())
    {
        int candyID;
        std::string candyName;

        try {
            std::getline(infile,inputText);
            std::stringstream ss;
            ss << inputText;

            ss >> candyID;
            ss >> candyName;

        }
        catch( ... ) {
            break;
        }

        boost::shared_ptr<Candy> candy(new Candy(candyID,candyName));

        if(m_candy.find(candyID) == m_candy.end()) {
            std::cout << "Loading a candy" << std::endl;
            m_candy[candyID] = candy;
        }
    }

    infile.close();

    //Read Price

    infile.open(m_price100Path.c_str());

    double price;

    //Read price
    {
        std::getline(infile,inputText);
        std::stringstream ss;
        ss << inputText;
        ss >> price;
    }
    m_price100 = price;
}

/**
 * @brief CandyDB::getContainer Get container from database
 * @param id the id of the container
 * @return the container as a shared pointer
 */
boost::shared_ptr<Container> CandyDB::getContainer(const size_t & id) const {
    ContainerMap::const_iterator it = m_container.find(id);
    if(it != m_container.end()) {
        return (*it).second;
    } else {
        boost::shared_ptr<Container> noContent;
        return noContent;
    }
}

/**
 * @brief CandyDB::getCandy Get candy from it's name
 * @param name the name to find
 * @return Candy as a shared pointer
 */
boost::shared_ptr<Candy> CandyDB::getCandy(const std::string & name) const {
    for(CandyMap::const_iterator it = m_candy.begin(); it != m_candy.end(); it++) {
        if( (*it).second->getCandyName() == name )
            return (*it).second;
    }

    boost::shared_ptr<Candy> noContent;
    return noContent;
}

/**
 * @brief CandyDB::getCandy Get candy from it's id
 * @param id The id of the candy
 * @return The Candy as a shared pointer
 */
boost::shared_ptr<Candy> CandyDB::getCandy(const size_t & id) const {
    CandyMap::const_iterator it = m_candy.find(id);
    if(it != m_candy.end()) {
        return (*it).second;
    } else {
        boost::shared_ptr<Candy> noContent;
        return noContent;
    }
}

/**
 * @brief CandyDB::getCandyAmount Get total amount of a specific candy type
 * @param id The candy type id
 * @return amount of candy in g
 */
double CandyDB::getCandyAmount(const size_t & id) const {
    double amount = 0;
    for(ContainerMap::const_iterator it = m_container.begin(); it != m_container.end(); it++) {
        if( (*it).first == id )
            amount += (*it).second->getCandyAmount();
    }

    return amount;
}

/**
 * @brief CandyDB::setContainerAmount Set the amount a container contains
 * @param id The id of the container
 * @param amount the amount in the container
 */
void CandyDB::setContainerAmount(const size_t & id, const double & amount) {
    boost::shared_ptr<Container> con = this->getContainer(id);
    if(con)
        con->setCandyAmount(amount);
}

/**
 * @brief CandyDB::getContainers Get all containers
 * @return A vector of all the containers in shared pointers
 */
CandyDB::ContainerList CandyDB::getContainers() const {
    ContainerList toReturn;
    for(ContainerMap::const_iterator it = m_container.begin(); it != m_container.end(); it++) {
        toReturn.push_back( (*it).second );
    }

    return toReturn;
}

/**
 * @brief CandyDB::getContainers Get all containers with a specific candy type
 * @param candyID The id of the candy to find
 * @return Vector of containers with a specific candy type
 */
CandyDB::ContainerList CandyDB::getContainers(size_t candyID) const {
    ContainerList toReturn;
    for(ContainerMap::const_iterator it = m_container.begin(); it != m_container.end(); it++) {
        if( candyID == (*it).second->getContents() )
            toReturn.push_back( (*it).second );
    }

    return toReturn;
}

/**
 * @brief CandyDB::getCandies Get all candies
 * @return Vector of Candy as shared pointers
 */
CandyDB::CandyList CandyDB::getCandies() const {
    CandyList toReturn;
    for(CandyMap::const_iterator it = m_candy.begin(); it != m_candy.end(); it++) {
        toReturn.push_back( (*it).second );
    }

    return toReturn;
}

/**
 * @brief CandyDB::getCandies Get all candies
 * @return Vector of Candy as shared pointers
 */
CandyDB::CandyList CandyDB::getUsedCandies() const {
    std::list<size_t> candyID;

    //fill list with used candy id's
    for(ContainerMap::const_iterator it = m_container.begin(); it != m_container.end(); it++) {
        candyID.push_back( (*it).second->getContents() );
    }

    //Sort id's
    candyID.sort();

    //Remove duplicates
    candyID.unique();

    CandyList toReturn;
    for(std::list<size_t>::const_iterator it = candyID.begin(); it != candyID.end(); it++) {
        toReturn.push_back(getCandy(*it));
    }

    return toReturn;
}

/**
 * @brief CandyDB::getPrice100
 * @return Price for 100 g
 */
double CandyDB::getPrice100() const {
    return m_price100;
}

/**
 * @brief CandyDB::setPrice100
 * @param price
 */
void CandyDB::setPrice100(double price) {
    m_price100 = price;
}
