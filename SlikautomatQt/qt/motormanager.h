#ifndef MOTORMANAGER_H
#define MOTORMANAGER_H

#include "ServiceLoaderModes.h"
#include "candydb.h"

class MotorManager : public ServiceLoaderModes
{
    boost::shared_ptr<CandyDB> m_db;
    boost::shared_ptr<mediator::Publisher> m_pub;
public:
    MotorManager(boost::shared_ptr<CandyDB> candyDB);

    void run(boost::shared_ptr<mediator::Publisher> pub);

    void setSpeed(size_t container, double speed);
    void stopAll();
};

#endif // MOTORMANAGER_H
