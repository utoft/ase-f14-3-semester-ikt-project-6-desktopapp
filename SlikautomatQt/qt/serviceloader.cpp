#include "serviceloader.h"

//Boost
#include <boost/bind.hpp>

//mediator
#include <mediator/messages/Signal.hpp>

/**
 * @brief wrapperFunc Wraps a class to load with a publisher
 * @param var the object containing a run function
 * @param publisher the publisher
 */
void wrapperFunc(boost::shared_ptr<ServiceLoaderModes> var,boost::shared_ptr<mediator::Publisher> publisher) {
    var->run(publisher);
}

/**
 * @brief ServiceLoader::ServiceLoader Loads all classes in sequence calling subscribe and run
 * @param pub
 * @param sub
 */
ServiceLoader::ServiceLoader(boost::shared_ptr<mediator::Publisher> pub,
                             boost::shared_ptr<mediator::Subscriber> sub)
    : m_pub(pub), m_sub(sub) {
}

/**
 * @brief ServiceLoader::addClass Add another class that needs to be loaded
 * @param toInit The class that should be loaded
 */
void ServiceLoader::addClass(boost::shared_ptr<ServiceLoaderModes> toInit) {
    boost::weak_ptr<ServiceLoaderModes> weakP(toInit);
    m_toLoad.push_back(weakP);
}

/**
 * @brief ServiceLoader::exec Go through all the subscribtion and run functions.
 * Spawning run in another thread if requested by the class
 */
void ServiceLoader::exec() {
    //Subscription

    //Subscribe to shutdown messages
    //m_sub->Subscribe<mediator::Signal>(boost::bind(&ServiceLoader::handleShutdown, this));

    //Call all classes's subscription methods
    for(ServiceList::iterator it = m_toLoad.begin();
        it != m_toLoad.end(); it++) {
        boost::shared_ptr<ServiceLoaderModes> var = (*it).lock();
        if(var)
            var->subscription(m_sub);
    }

    //Call all classes's run methods. Sometimes inside threads
    for(ServiceList::iterator it = m_toLoad.begin();
        it != m_toLoad.end(); it++) {
        boost::shared_ptr<ServiceLoaderModes> var = (*it).lock();
        if(var) {
            if(var->m_runThreaded) {
                std::cout << "Adding thread" << std::endl;
                boost::thread * thread = new boost::thread(wrapperFunc,var,m_pub);
                m_threads.add_thread(thread);
            }
            else
                var->run(m_pub);
        }
    }
}

/**
 * @brief ServiceLoader::handleShutdown when called the system will initiate system shutdown
 */
void ServiceLoader::handleShutdown() {
    for(ServiceList::iterator it = m_toLoad.begin();
        it != m_toLoad.end(); it++) {
        boost::shared_ptr<ServiceLoaderModes> var = (*it).lock();
        if(var) {
            var->cleanup();
        }
    }

    m_threads.join_all();
}
