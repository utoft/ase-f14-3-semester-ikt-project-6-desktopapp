#include "ordermanager.h"
#include "GenericMessage.h"

#include <mediator/messages/Signal.hpp>

/**
 * @brief Ordermanager::subscription Subscribes to "/order/done" so we know when order is done
 * @param sub
 */
void Ordermanager::subscription(boost::shared_ptr<mediator::Subscriber> sub) {
    m_subscription = sub->Subscribe<mediator::Signal>("/order/done",boost::bind(&Ordermanager::handleDone,this));
}

/**
 * @brief Ordermanager::run Used to get the publisher which is needed in the sendStart method
 * @param publisher
 */
void Ordermanager::run(boost::shared_ptr<mediator::Publisher> publisher) {
    m_pub = publisher;
}

/**
 * @brief Ordermanager::sendStart Sends message that will make a daemon start feeding candy from containers.
 */
void Ordermanager::sendStart() {
    map< size_t, double > listToSend; //The order we want executed
    //TODO Fill map!

    //Iterate through and show on list
    for(std::map<size_t, double>::const_iterator it = orderMap_.begin(); it != orderMap_.end(); it++) {
        listToSend.insert(std::pair<size_t, double>((*it).first, (*it).second) );
    }

    //The map should contain
    //1: ID of candy
    //2: amount of candy in g

    //The message we send (just like sending to a MsgQueue)
    boost::shared_ptr< GenericMessage< map< size_t, double > > > msg( new GenericMessage< map< size_t, double > >(listToSend));

    m_pub->Publish("/order/start",msg); //Send Message.
}

/**
 * @brief Ordermanager::handleDone will be called by daemon when the requested candy have been feeded to the candy bag
 */
void Ordermanager::handleDone() {
    //What will you do when the order have finished?
    //TODO
    orderMap_.clear();
}

/**
 * @brief Ordermanager::setCandyID
 * @param id
 */
void Ordermanager::setCandyID(std::string id){
    candyID_ = id;
}

/**
 * @brief Ordermanager::addToMap
 * @param candyID
 * @param amount
 */
void Ordermanager::addToMap(size_t candyID, double amount){
    orderMap_[candyID] = amount;
}

/**
 * @brief Ordermanager::getMap
 * @return
 */
std::map<size_t, double> Ordermanager::getMap() const{
    return orderMap_;
}

/**
 * @brief Ordermanager::setAmount
 * @param amount
 */
void Ordermanager::setAmount(double amount) {
    amount_ = amount;
}

/**
 * @brief Ordermanager::getAmount
 * @return
 */
double Ordermanager::getAmount() const{
    return amount_;
}

/**
 * @brief Ordermanager::getCandyID
 * @return
 */
std::string Ordermanager::getCandyID() const{
    return candyID_;
}
/**
 * @brief Ordermanager::setPrice
 * @param price
 */
void Ordermanager::setPrice(double price){
    price_ += price;
}

/**
 * @brief Ordermanager::getPrice
 * @return price for order
 */
double Ordermanager::getPrice() const{
   return price_;
}

/**
 * @brief Ordermanager::clearOrder
 *Clears the order (resets)
 */
void Ordermanager::clearOrder(){
    candyID_ = "";
    amount_ = 0.0;
    price_ = 0.0;
    orderMap_.clear();
}


double Ordermanager::getTotalAmount() const{
    double sum = 0;
    for (std::map<size_t, double>::const_iterator it = orderMap_.begin(); it != orderMap_.end(); it++) {
        sum += ((*it).second);
    }

    return sum;
}
