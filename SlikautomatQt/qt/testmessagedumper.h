#ifndef TESTMESSAGEDUMPER_H
#define TESTMESSAGEDUMPER_H

#include <iostream>
#include <list>


#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

#include <mediator/Publisher.hpp>
#include <mediator/messages/Signal.hpp>

#include "ServiceLoaderModes.h"
#include "GenericMessage.h"

class TestMessageDumper : public ServiceLoaderModes
{
public:
    TestMessageDumper(boost::shared_ptr<mediator::Subscriber> sub,mediator::MessageIdentifier id, std::string nameSpecifier)
        : m_id(id), m_identifier(nameSpecifier), m_subscriber(sub) {}

    template<typename Type>
    void addType() {
        subscriptions.push_back(m_subscriber->Subscribe< Type >(m_id,boost::bind( &TestMessageDumper::msgHandler,this, _1, _2)));
    }

    void msgHandler(const mediator::MessageIdentifier& id, boost::shared_ptr<mediator::Message> message) {
        std::cout << m_identifier << ": " << id.toString() << std::endl;
    }

private:
    mediator::MessageIdentifier m_id;
    std::string m_identifier;
    std::list< boost::shared_ptr<const mediator::Subscription> > subscriptions;
    boost::shared_ptr<mediator::Subscriber> m_subscriber;
};

#endif // TESTMESSAGEDUMPER_H

#include <boost/bimap.hpp>
