#ifndef DAEMONBAGINSERTED_H
#define DAEMONBAGINSERTED_H

#include "ServiceLoaderModes.h"

class DaemonBagInserted : public ServiceLoaderModes
{
    boost::shared_ptr<mediator::Publisher> m_pub;
    std::list< boost::shared_ptr<const mediator::Subscription> > m_subscriptions;

public:
    DaemonBagInserted();

    void subscription(boost::shared_ptr<mediator::Subscriber> sub);

    void run(boost::shared_ptr<mediator::Publisher> pub);

    void handleBagChanged(boost::shared_ptr<mediator::Message> message);
};

#endif // DAEMONBAGINSERTED_H
