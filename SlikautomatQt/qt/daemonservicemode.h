#ifndef DAEMONSERVICEMODE_H
#define DAEMONSERVICEMODE_H

#include "ServiceLoaderModes.h"

#include <boost/chrono.hpp>

class DaemonServiceMode : public ServiceLoaderModes
{
    bool serviceOn;
    boost::shared_ptr<mediator::Publisher> pub;
    boost::shared_ptr<const mediator::Subscription> subscrip;

public:
    DaemonServiceMode(bool serviceModeOn = false);

    void subscription(boost::shared_ptr<mediator::Subscriber> sub);

    void run(boost::shared_ptr<mediator::Publisher> publisher);

    void handleChanged();
};

#endif // DAEMONSERVICEMODE_H
