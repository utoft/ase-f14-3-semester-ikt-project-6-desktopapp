#include "servicemode.h"
#include "ui_servicemode.h"

#include <QDebug>

/**
 * @brief Servicemode::Servicemode
 * @param candyDB Database with information about candy, containers and price.
 * @param motorManager Can control motor speed, start and stop them.
 * @param parent Not used Need to be NULL
 */
Servicemode::Servicemode(boost::shared_ptr<CandyDB> candyDB, boost::shared_ptr<MotorManager> motorManager, QWidget *parent) :
    QDialog(parent)
  , candyDB_(candyDB)
  , m_motorManager(motorManager)
  , ui(new Ui::Servicemode)
  , cleanupActive_(false)
  , currentManualMotor_(0)
  , motorActive_(false)
{
    ui->setupUi(this);
    ui->stackedWidgetServicemode->setCurrentWidget(ui->pageServiceMenu);

    //Bindings
}

/**
 * @brief Servicemode::~Servicemode
 */
Servicemode::~Servicemode()
{
    delete ui;
}

/**
 * @brief Servicemode::on_refillContainer_clicked
 */
void Servicemode::on_refillContainer_clicked()
{
    ui->stackedWidgetServicemode->setCurrentWidget(ui->pageRefillContainer);
    //TODO: evt. stop all motors or do something so the system wont startup unintentionally.
}

/**
 * @brief Servicemode::on_doneRefillContainer_clicked
 */
void Servicemode::on_doneRefillContainer_clicked()
{
    ui->stackedWidgetServicemode->setCurrentWidget(ui->pageServiceMenu);
}

/**
 * @brief Servicemode::on_cleanUp_clicked
 *List current containers using the CandyDB-class
 */
void Servicemode::on_cleanUp_clicked()
{
    //Get containers
    CandyDB::ContainerList containers = candyDB_->getContainers();

    //Iterate and show on GUI list
    for (CandyDB::ContainerList::const_iterator it=containers.begin(); it != containers.end(); it++){
        ui->containerList->addItem(QString::number(static_cast<int>((*it)->getContainerID())));
    }

    //Set first row in list as standard
    ui->containerList->setCurrentRow(0);

    //Change view
    ui->stackedWidgetServicemode->setCurrentWidget(ui->pageCleanUp);
    cleanupActive_ = true;
}

/**
 * @brief Servicemode::on_pushButtonCleanUpBack_clicked
 *List servicemenu on back button
 */
void Servicemode::on_pushButtonCleanUpBack_clicked()
{
    cleanupActive_ = false;
    //Clear GUI list
    ui->containerList->clear();

    m_motorManager->stopAll();

    //Show servicemode menu
    Servicemode::on_doneRefillContainer_clicked();
}

/**
 * @brief Servicemode::on_startMotor_clicked
 *Start motor when clicked
 */
void Servicemode::on_startMotor_clicked()
{
    if(!cleanupActive_)
        return;
    boost::shared_ptr<Container> container = candyDB_->getContainer(currentManualMotor_);

    motorActive_ = true;

    if(container) {
        currentMotorSpeed_ = container->getManualSpeed();
        ui->verticalSlider->setValue(currentMotorSpeed_ * 100);
    }

    if(currentManualMotor_ != 0)
        m_motorManager->setSpeed(currentManualMotor_, currentMotorSpeed_);

    std::cout << "Manual Motor: " << "started" << std::endl;
}

/**
 * @brief Servicemode::on_stopMotor_clicked
 *Stop motor when clicked
 */
void Servicemode::on_stopMotor_clicked()
{
    if(!cleanupActive_)
        return;
    motorActive_ = false;
    setSpeed(0);
    ui->verticalSlider->setValue(0);

    if(currentManualMotor_ != 0)
        m_motorManager->setSpeed(currentManualMotor_ ,0);

    std::cout << "Manual Motor: " << "stopped" << std::endl;
}

void Servicemode::on_verticalSlider_valueChanged(int value)
{
    if(!cleanupActive_)
        return;
    currentMotorSpeed_ = (double)(value) / 100;

    if(motorActive_)
        m_motorManager->setSpeed(currentManualMotor_, currentMotorSpeed_);
    std::cout << "Manual Motor: " << "speed changed to" << currentMotorSpeed_ << std::endl;
}


void Servicemode::on_containerList_currentRowChanged(int currentRow)
{
    if(!cleanupActive_)
        return;
    QString textId = ui->containerList->currentItem()->text();
    stringstream ss;
    ss << textId.toStdString();
    size_t id;
    ss >> id;

    currentManualMotor_ = id;

    std::cout << "Manual Motor: " << "chosen" << std::endl;
}

void Servicemode::setSpeed(int value) {
    currentMotorSpeed_ = (double)(value) / 100;
}

/**
 * @brief Servicemode::on_changeCandy_clicked
 *Change widget-view to change candy. Show container and candy info
 */
void Servicemode::on_changeCandy_clicked()
{
    m_changeCandyLoading = true;
    //Change widget-view to change candy
    ui->stackedWidgetServicemode->setCurrentWidget(ui->pageChangeCandy);

    //Clear lists
    ui->listContainerID->clear();
    ui->listCandyID->clear();

    //Show containers and candy on lists
    CandyDB::ContainerList containers = candyDB_->getContainers();
    CandyDB::CandyList candys = candyDB_->getCandies();

    //Iterate and show containers on GUI list
    for (CandyDB::ContainerList::const_iterator it=containers.begin(); it != containers.end(); it++){
        ui->listContainerID->addItem(QString::number(static_cast<int>((*it)->getContainerID())));
    }

    //Set first row in lists as standard
    ui->listContainerID->setCurrentRow(0);

    //First container element
    boost::shared_ptr<Container> cont = *containers.begin();
    size_t candyID = cont->getContents();

    //Iterate and show candy on GUI list
    for (CandyDB::CandyList::const_iterator it=candys.begin(); it != candys.end(); it++){
        ui->listCandyID->addItem(QString::fromStdString((*it)->getCandyName()));
        if( (*it)->getID() == candyID ) { //Set the last added item as the active item
            const int itemCount = ui->listCandyID->count();
            QListWidgetItem* item = ui->listCandyID->item( itemCount - 1 ); //The last item
            ui->listCandyID->setCurrentItem(item);
        }
    }
    m_changeCandyLoading = false;
}

/**
 * @brief Servicemode::on_listContainerID_currentRowChanged
 */
void Servicemode::on_listContainerID_currentRowChanged(int currentRow)
{
    if(m_changeCandyLoading)
        return;
    int id = ui->listContainerID->currentItem()->text().toInt();

    boost::shared_ptr<Container> cont = candyDB_->getContainer(id);
    size_t candyID = cont->getContents();
    boost::shared_ptr<Candy> candy = candyDB_->getCandy(candyID);
    string candyName = candy->getCandyName();

    //Iterate though all items in list
    for (int i = 0; i < ui->listCandyID->count(); i++){
        QListWidgetItem* item = ui->listCandyID->item(i); //Get item
        if(item->text().toStdString() == candyName) { //check if this item is the correct one
            ui->listCandyID->setCurrentRow(i); //Mark the current item
            break;
        }
    }
}

void Servicemode::on_listCandyID_currentRowChanged(int currentRow)
{
    if(m_changeCandyLoading)
        return;

    //Get chosen candyID
    QListWidgetItem* item = ui->listCandyID->currentItem();

    //Find matching Candy in db
    boost::shared_ptr<Candy> candy = candyDB_->getCandy(item->text().toStdString());
    size_t wantedID = candy->getID();

    //Get container id
    int containerID = ui->listContainerID->currentItem()->text().toInt();
    boost::shared_ptr<Container> cont = candyDB_->getContainer(containerID);

    //Find candyID from container
    size_t currentID = cont->getContents();

    //Check if new candy type is chosen
    if(currentID != wantedID)
        m_wantedContainerCandy[cont->getContainerID()] = wantedID;
}

/**
 * @brief Servicemode::on_pushButtonChangeCandyOk_clicked
 */
void Servicemode::on_pushButtonChangeCandyOk_clicked()
{
    //Save current items to db
    for(map< size_t, size_t>::const_iterator it = m_wantedContainerCandy.begin(); it != m_wantedContainerCandy.end(); it++) {
        pair< size_t, size_t> element = *it;
        candyDB_->getContainer(element.first)->setContents(element.second);
    }

    //Reset change list
    m_wantedContainerCandy.clear();
    candyDB_->save();
}

/**
 * @brief Servicemode::on_pushButtonChangeCandyBack_clicked
 */
void Servicemode::on_pushButtonChangeCandyBack_clicked()
{
    //Reset change list
    m_wantedContainerCandy.clear();

    //Go menu page
    ui->stackedWidgetServicemode->setCurrentWidget(ui->pageServiceMenu);
}

/**
 * @brief Servicemode::on_changePrice_clicked
 */
void Servicemode::on_changePrice_clicked()
{
    //Show change price widget
    ui->stackedWidgetServicemode->setCurrentWidget(ui->pageChangePrice);

    //Show current price
    QString qs = QString::number(candyDB_->getPrice100());
    ui->labelCurrentPrice->setText(qs + " kr.");

    //Reset spinbox to current price
    ui->doubleSpinBoxPrice->setValue(candyDB_->getPrice100());

}

/**
 * @brief Servicemode::show_window
 * @param fullscreen
 * Bool for showing window in fulscreen or not
 */
void Servicemode::show_window(bool fullscreen) {
    if(fullscreen)
        this->showFullScreen();
    else
        this->show();
}

/**
 * @brief Servicemode::close_window
 * Closes window (the whole stacked widget)
 */
void Servicemode::close_window() {
    this->close();
}

/**
 * @brief Servicemode::on_pushButtonAnulChangePrice_clicked
 */
void Servicemode::on_pushButtonAnulChangePrice_clicked()
{
    //Cancel change price
    ui->stackedWidgetServicemode->setCurrentWidget(ui->pageServiceMenu);

    //Reset spinbox
    ui->doubleSpinBoxPrice->setValue(0.00);
}

void Servicemode::on_pushButtonAcceptPrice_clicked()
{
    //Get value from spinbox and assign to price
    candyDB_->setPrice100(ui->doubleSpinBoxPrice->value());

    //Show main window
    ui->stackedWidgetServicemode->setCurrentWidget(ui->pageServiceMenu);
}
