#ifndef ORDERMANAGER_H
#define ORDERMANAGER_H

#include <map>
#include <string>
#include "ServiceLoaderModes.h"

class Ordermanager : public ServiceLoaderModes{
public:
    Ordermanager() : candyID_("None"), amount_(0.0), price_(0.0) {}
    void setCandyID(std::string candyID);
    std::string getCandyID() const;
    void setAmount(double amount);
    double getAmount() const;
    double getTotalAmount() const;
    void setPrice(double price);
    double getPrice() const;
    void addToMap(size_t candyID, double amount);
    void clearOrder();
    std::map<size_t, double> getMap() const;

    void subscription(boost::shared_ptr<mediator::Subscriber> sub);

    void run(boost::shared_ptr<mediator::Publisher> publisher);

    void sendStart();
    void handleDone();
private:
    std::string candyID_;
    double amount_;
    double price_;
    std::map<size_t, double> orderMap_;
    boost::shared_ptr<mediator::Publisher> m_pub;
    boost::shared_ptr<const mediator::Subscription> m_subscription;
};

#endif // ORDERMANAGER_H
