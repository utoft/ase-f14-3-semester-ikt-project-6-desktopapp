#ifndef PSOCFEEDCONTAINER_H
#define PSOCFEEDCONTAINER_H

#include <list>

#include "ServiceLoaderModes.h"

class PSoCFeedContainer : public ServiceLoaderModes
{
    std::list< boost::shared_ptr<const mediator::Subscription> > m_subscriptions;
    std::string m_writePath;
public:
    PSoCFeedContainer(std::string conPath);

    void subscription(boost::shared_ptr<mediator::Subscriber> sub);

    void handleSendAmount(boost::shared_ptr<mediator::Message> message);
};

#endif // PSOCFEEDCONTAINER_H
