#include <fstream>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <QDebug>
#include "Container.h"

/**
 * @brief Container::Container Create container
 * @param id The id of the container
 */
Container::Container(const size_t & id)
    : containerID_(id)
    , candyAmount_(0)
    , candyID(0)
    , maxSpeed_(0)
{}

/**
 * @brief Container::getContainerID Get container id
 * @return the id of the container
 */
int Container::getContainerID() const{
    return containerID_;
}

/**
 * @brief Container::getCandyAmount Get current amount in container
 * @return the amount in the container
 */
double Container::getCandyAmount() const{
      //Input lower api function here that gets the amount in container and returns this (in procent)
    int procentVal = 46; // For test purposes it's set to 46 (%)
    return procentVal;
}

/**
 * @brief Container::setContents Set the type of candy contained in container
 * @param id the Id of the candy type
 */
void Container::setContents(const size_t & id){
    candyID=id;
}

/**
 * @brief Container::getContents Get current candy type in container
 * @return id of current candy type
 */
size_t Container::getContents() const{
    return candyID;
}

/**
 * @brief Container::setCandyAmount Set current amount of candy in container
 * @param amount the current amount in the container
 */
void Container::setCandyAmount(const double & amount) {
    candyAmount_ = amount;
}

/**
 * @brief Container::getManualSpeed Manual speed for this container
 * @return the current default speed
 */
double Container::getManualSpeed() {
    return maxSpeed_;
}

/**
 * @brief Container::setManualSpeed Change manual speed for this container
 * @param speed The speed to save
 */
void Container::setManualSpeed(const double & speed) {
    maxSpeed_ = speed;
}
