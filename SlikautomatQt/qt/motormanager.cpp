#include "motormanager.h"

#include "GenericMessage.h"

MotorManager::MotorManager(boost::shared_ptr<CandyDB> candyDB)
    : m_db(candyDB)
{}

void MotorManager::run(boost::shared_ptr<mediator::Publisher> pub) {
    m_pub = pub;
}

void MotorManager::setSpeed(size_t container, double speed) {
    typedef pair<size_t,double> MsgContent;
    typedef GenericMessage< MsgContent > MessageType;

    MsgContent content(container,speed);

    MessageType * msgPtr = new MessageType(content);
    boost::shared_ptr< MessageType > message(msgPtr);
    m_pub->Publish("/motor/speed/change",message);
}

void MotorManager::stopAll() {
    CandyDB::ContainerList containers = m_db->getContainers();

    //To stop all motors we only need to send a stop signal to one motor
    size_t id = (*containers.begin())->getContainerID();
    setSpeed(id,0);

    //for(CandyDB::ContainerList::const_iterator it = containers.begin(); it != containers.end(); it++) {
    //    size_t id = (*it)->getContainerID();
    //    setSpeed(id,0);
    //}
}
