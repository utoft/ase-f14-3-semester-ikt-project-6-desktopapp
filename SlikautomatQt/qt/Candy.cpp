#include "Candy.h"

/**
 * @brief Candy::Candy
 * @param id ID given to candy type
 * @param name The name of the candy
 */
Candy::Candy(size_t id, std::string name)
    : m_id(id), m_name(name) {
}

/**
 * @brief Candy::getCandyName Get the name of a candy type
 * @return the name of the candy
 */
std::string Candy::getCandyName() const{
    return m_name;
}

size_t Candy::getID() const{
    return m_id;
}
