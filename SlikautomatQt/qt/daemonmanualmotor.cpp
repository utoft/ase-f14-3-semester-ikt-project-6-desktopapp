#include "daemonmanualmotor.h"


DaemonManualMotor::DaemonManualMotor(const int & speedSteps)
    : m_speedSteps(speedSteps)
{
}


void DaemonManualMotor::subscription(boost::shared_ptr<mediator::Subscriber> sub) {
    m_subscription = sub->Subscribe < MsgRecieveType >("/motor/speed/change", boost::bind(&DaemonManualMotor::handleSetSpeed, this, _2));
}

void DaemonManualMotor::run(boost::shared_ptr<mediator::Publisher> pub) {
    m_pub = pub;
}

void DaemonManualMotor::handleSetSpeed(boost::shared_ptr<mediator::Message> message) {
    boost::shared_ptr< MsgRecieveType > inputMsg = boost::static_pointer_cast< MsgRecieveType >(message);
    size_t motorID = inputMsg->getValue().first;
    double speed = inputMsg->getValue().second;

    int steppedSpeed = speed*m_speedSteps;

    if(steppedSpeed > m_speedSteps-1)
        steppedSpeed = m_speedSteps-1;
    else if(steppedSpeed < 0)
        steppedSpeed = 0;

    boost::shared_ptr< MsgSendType > outputMsg(new MsgSendType(steppedSpeed));

    stringstream msgBuild;
    msgBuild << "/psoc/canister/motor/";
    msgBuild << motorID;

    //Send it
    m_pub->Publish( msgBuild.str(), outputMsg);
}
