#ifndef SERVICEMODE_H
#define SERVICEMODE_H

#include <QDialog>

#include "candydb.h"
#include "motormanager.h"

#include <QListWidgetItem>

namespace Ui {
class Servicemode;
}

class Servicemode : public QDialog
{
    Q_OBJECT

public slots:
    void show_window(bool fullscreen);
    void close_window();

public:
    explicit Servicemode(boost::shared_ptr<CandyDB> candyDB, boost::shared_ptr<MotorManager> motorManager, QWidget *parent = 0);
    ~Servicemode();
    
private slots:
    void on_refillContainer_clicked();

    void on_doneRefillContainer_clicked();

    void on_cleanUp_clicked();

    void on_pushButtonCleanUpBack_clicked();

    void on_startMotor_clicked();

    void on_stopMotor_clicked();

    void on_changeCandy_clicked();

    void on_pushButtonChangeCandyOk_clicked();

    void on_changePrice_clicked();

    void on_pushButtonChangeCandyBack_clicked();

    void on_pushButtonAnulChangePrice_clicked();

    void on_listContainerID_currentRowChanged(int currentRow);

    void on_listCandyID_currentRowChanged(int currentRow);
    void on_pushButtonAcceptPrice_clicked();

    void on_containerList_currentRowChanged(int currentRow);

    void on_verticalSlider_valueChanged(int value);

private:
    void setSpeed(int value);

    Ui::Servicemode *ui;
    boost::shared_ptr<CandyDB> candyDB_;
    boost::shared_ptr<MotorManager> m_motorManager;
    bool m_changeCandyLoading;
    std::map<size_t, size_t> m_wantedContainerCandy;

    bool cleanupActive_;
    int currentManualMotor_;
    double currentMotorSpeed_;
    bool motorActive_;
};

#endif // SERVICEMODE_H
