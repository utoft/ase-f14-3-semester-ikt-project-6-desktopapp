#ifndef SERVICELOADER_H
#define SERVICELOADER_H

//STL
#include <vector>
#include <map>

//Boost

#include <boost/weak_ptr.hpp>
#include <boost/thread.hpp>

//Mediator
#include <mediator/Publisher.hpp>
#include <mediator/Subscriber.hpp>

//Local
#include "ServiceLoaderModes.h"

/**
 * @brief The ServiceLoader class
 *
 * ServiceLoader calls a pre given list of functions to ensure classes is loaded in the correct order.
 */
class ServiceLoader
{
    typedef boost::thread_group ThreadMap;
    typedef std::vector< boost::weak_ptr<ServiceLoaderModes> > ServiceList;
public:
    ServiceLoader(boost::shared_ptr<mediator::Publisher> pub, boost::shared_ptr<mediator::Subscriber> sub);

    /**
     * @brief addClass Adds a class to be loaded
     * @param toInit The class to init
     */
    void addClass(boost::shared_ptr<ServiceLoaderModes> toInit);

    /**
     * @brief exec Run the initialization.
     */
    void exec();

    void handleShutdown();

private:
    boost::shared_ptr< mediator::Publisher > m_pub;
    boost::shared_ptr< mediator::Subscriber > m_sub;
    ServiceList m_toLoad;
    ThreadMap m_threads;
};

#endif // SERVICELOADER_H
