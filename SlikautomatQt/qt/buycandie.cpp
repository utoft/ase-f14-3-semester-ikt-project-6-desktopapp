#include "buycandie.h"
#include "ui_buycandie.h"

#include <iostream>
#include <QString>
#include <QDebug>
#include <QTimer>

#include <vector>
#include <iomanip>

/**
 * @brief Buycandie::Buycandie
 * @param orderMan
 * @param candyDB
 * @param parent
 * Default constructor with member initializer list
 */
Buycandie::Buycandie(boost::shared_ptr<Ordermanager> orderMan, boost::shared_ptr<CandyDB> candyDB,QWidget *parent) :
    QDialog(parent)
  , ui(new Ui::Buycandie)
  , m_db(candyDB)
  , orderMan_(orderMan)
  , waitingForBag_(false)
  , bagPresent_(false)
  , orderIsPacking_(false)
  , m_currentInsertedMoney(0)
{
    ui->setupUi(this);
    ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pageBuyCandieMain);
}

/**
 * @brief Buycandie::~Buycandie
 * Destructor
 */
Buycandie::~Buycandie()
{
    delete ui;
}

/**
 * @brief Buycandie::on_buyCandieButton_clicked
 * When button clicked initliaze page choose candy, and show list on GUI
 */
void Buycandie::on_buyCandieButton_clicked()
{
    //Activate next page
    ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pageChooseCandy);

    //Show list of candy on the next page when clicked

    //Read from conf-file
    CandyDB::CandyList candies = m_db->getUsedCandies();

    ui->listWidget->clear();

    //Iterate through and show on list
    for(CandyDB::CandyList::const_iterator it = candies.begin(); it != candies.end(); it++) {
        boost::shared_ptr<Candy> candy = *it;
        std::cout << candy->getCandyName() << std::endl;
        ui->listWidget->addItem(QString::fromStdString((*it)->getCandyName()));
    }

    //Set first row in list as standard
    ui->listWidget->setCurrentRow(0);
}

/**
 * @brief Buycandie::on_annullerButton_clicked
 * Cancel button which clears order and returns to buy candie main page
 */
void Buycandie::on_annullerButton_clicked()
{
    //Clear order
    orderMan_->clearOrder();

    //Clear GUI list (order)
    ui->listWidgetOrderTotal->clear();

    //Show main-page
    ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pageBuyCandieMain);
}

/**
 * @brief Buycandie::show_window
 * @param fullscreen
 * Bool for showing window in fulscreen or not
 */
void Buycandie::show_window(bool fullscreen) {
    if(fullscreen)
        this->showFullScreen();
    else
        this->show();
}

/**
 * @brief Buycandie::change_main
 */
void Buycandie::change_main(){
    ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pageBuyCandieMain);
}

/**
 * @brief Buycandie::close_window
 * Closes window (the whole stacked widget)
 */
void Buycandie::close_window() {
    this->close();
}

/**
 * @brief Buycandie::bag_not_inserted
 */
void Buycandie::bag_not_inserted(){
    bagPresent_ = false;
}

/**
 * @brief Buycandie::bag_inserted
 */
void Buycandie::bag_inserted(){
    bagPresent_ = true;
    if (waitingForBag_){
        this->orderStart();
    }
}

/**
 * @brief Buycandie::order_done
 */
void Buycandie::order_done() {
    if(orderIsPacking_) {
        ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pageBuyCandieDone);
        QTimer::singleShot(3000, this, SLOT(change_main()));
        orderIsPacking_ = false;
    }
}

/**
 * @brief Buycandie::orderStart
 */
void Buycandie::orderStart(){
    orderIsPacking_ = true;
    orderMan_->sendStart();
    ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pagePackingOrder);
}

/**
 * @brief Buycandie::payment_event
 * @param money
 */
void Buycandie::payment_event(int money){
    m_currentInsertedMoney = money;
    ui->labelShowPaidAmount->setText("Der er betalt: " + QString::number(money) + " kr.");

    checkPayment();
}

/**
 * @brief Buycandie::on_pushButtonNextCandyType_clicked
 * Choose candy-type and assign to order
 */
void Buycandie::on_pushButtonNextCandyType_clicked()
{
    //Choose candy-type and assign to ordermanager
    QString qs= ui->listWidget->currentItem()->text();
    orderMan_->setCandyID(qs.toStdString());
    ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pageChooseAmount);
    ui->labelChosenCandyType->setText(qs);

    //Clear string
    qs.clear();
}

/**
 * @brief Buycandie::on_pushButtonNextAmount_clicked
 * Add amount to order, then show current order-info on display
 */
void Buycandie::on_pushButtonNextAmount_clicked()
{
    //Add the order to stl-map private member
    boost::shared_ptr<Candy> cd = m_db->getCandy(orderMan_->getCandyID());
    orderMan_->setAmount(static_cast<double> (ui->spinBoxAmount->value()) );
    orderMan_->addToMap((cd->getID()), orderMan_->getAmount());

    //Change window
    ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pagePayOrMore);

    //Clear GUI list (order)
    ui->listWidgetOrderTotal->clear();

    //Show current orders on new display
    std::map<size_t, double> om = orderMan_->getMap();

    //Iterate through and show on list
    for (std::map<size_t, double>::const_iterator it = om.begin(); it != om.end(); it++) {

        boost::shared_ptr<Candy> cd = m_db->getCandy((*it).first);

        QString qsID = QString::fromStdString(cd->getCandyName());
        QString qsAmount = QString::number((*it).second);

        //Make string to show
        QString qs = qsID + "\t" + qsAmount + " g.";
        ui->listWidgetOrderTotal->addItem(qs);
    }

    //Check if payment is enough
    checkPayment();
}

/**
 * @brief Buycandie::on_pushButtonAnuller_clicked
 */
void Buycandie::on_pushButtonAnuller_clicked()
{
    //Use already created cancel button
    Buycandie::on_annullerButton_clicked();
}

/**
 * @brief Buycandie::on_pushButtonAnullerAmount_clicked
 */
void Buycandie::on_pushButtonAnullerAmount_clicked()
{
    //Use already created cancel button
    Buycandie::on_annullerButton_clicked();
}

/**
 * @brief Buycandie::on_pushButtonAnullerPayOrMore_clicked
 */
void Buycandie::on_pushButtonAnullerPayOrMore_clicked()
{
    //Use already created anulment button
    Buycandie::on_annullerButton_clicked();
}

/**
 * @brief Buycandie::on_pushButtonChooseMore_clicked
 */
void Buycandie::on_pushButtonChooseMore_clicked()
{
    //Go to "choose more candy-window
    ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pageChooseCandy);
}
/**
 * @brief Buycandie::on_pushButtonPay_clicked
 */
void Buycandie::on_pushButtonPay_clicked()
{
    //Calculate total amount
    double totalGrams = orderMan_->getTotalAmount();

    //Make string to show grams
    QString qsAmount = "Gram: " + QString::number(totalGrams) + " g.";
    ui->labelShowTotalGrams->setText(qsAmount);

    //Calculate price
    double price = 0.0;
    price = (m_db->getPrice100() / 100) * totalGrams;

    //Make string to show grams
    QString qsPrice = "Pris total: " + QString::number(price) + " kr.";
    ui->labelShowTotalPrice->setText(qsPrice);

    //Change widget window
    ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pageShowTotalInfoAndPay);

}

/**
 * @brief Buycandie::on_pushButtonCancelOrder_clicked
 */
void Buycandie::on_pushButtonCancelOrder_clicked()
{
    //Clear order variables
    orderMan_->clearOrder();

    //Show main widget window
    ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pageBuyCandieMain);
}

void Buycandie::on_pushButtonPayOrder_clicked()
{
    if (bagPresent_)
        this->orderStart();
    else {
        waitingForBag_ = true;
        ui->stackedWidgetBuyCandie->setCurrentWidget(ui->pageInsertBag);
    }
}


void Buycandie::checkPayment()
{
    //Check if level is too low
    if(orderMan_->getTotalAmount()*m_db->getPrice100()/100 > m_currentInsertedMoney) {
        ui->pushButtonPayOrder->setEnabled(false);
        ui->pushButtonPayOrder->setDisabled(true);
    } else {
        ui->pushButtonPayOrder->setEnabled(true);
        ui->pushButtonPayOrder->setDisabled(false);
    }
}
