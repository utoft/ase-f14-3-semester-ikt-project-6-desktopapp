#ifndef CONTAINER_H
#define CONTAINER_H

#include <string>
#include <QString>
#include "Candy.h"

class Container{
public:
    Container(const size_t & id);
    int getContainerID() const;
    double getCandyAmount() const;
    void setCandyAmount(const double & amount);
    size_t getContents() const;
    void setContents(const size_t & id);

    double getManualSpeed();
    void setManualSpeed(const double & speed);

private:
    size_t containerID_;
    size_t candyID;
    double candyAmount_;
    double maxSpeed_;
};

#endif // CONTAINER_H
