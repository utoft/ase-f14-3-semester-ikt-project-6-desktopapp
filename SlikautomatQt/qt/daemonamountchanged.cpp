#include "daemonamountchanged.h"
#include "GenericMessage.h"

/**
 * @brief DaemonAmountChanged::DaemonAmountChanged Constructor for Daemon
 * @param db Container Database containing amount in each container
 */
DaemonAmountChanged::DaemonAmountChanged(boost::shared_ptr<CandyDB> db) : m_db(db) {}

/**
 * @brief DaemonAmountChanged::subscription Subscripe to events about changes in canister levels
 * @param sub the Subscriber class to use.
 */
void DaemonAmountChanged::subscription(boost::shared_ptr<mediator::Subscriber> sub) {
    subscrip = sub->Subscribe< GenericMessage< pair<size_t,int> > >("/psoc/canister/amount/changed/",boost::bind( &DaemonAmountChanged::msgHandler, this, _1, _2));
}

/**
 * @brief DaemonAmountChanged::msgHandler Handles message events recieved.
 * @param id Id of message
 * @param message The message with the current amount
 */
void DaemonAmountChanged::msgHandler(const mediator::MessageIdentifier& id, boost::shared_ptr<mediator::Message> message) {
    boost::shared_ptr< GenericMessage< pair<size_t,int> > > msg = boost::static_pointer_cast< GenericMessage< pair<size_t,int> > >(message);

    m_db->setContainerAmount(msg->getValue().first, msg->getValue().second);
}
