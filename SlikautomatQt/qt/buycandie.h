#ifndef BUYCANDIE_H
#define BUYCANDIE_H

#include <QDialog>

#include <boost/shared_ptr.hpp>

#include "ordermanager.h"
#include "candydb.h"

namespace Ui {
class Buycandie;
}

class Buycandie : public QDialog
{
    Q_OBJECT

public slots:
    void show_window(bool fullscreen);
    void change_main();
    void close_window();
    void bag_not_inserted();
    void bag_inserted();
    void payment_event(int money);
    void order_done();
    
public:
    explicit Buycandie(boost::shared_ptr<Ordermanager> orderMan, boost::shared_ptr<CandyDB> candyDB, QWidget *parent = 0);
    void orderStart();
    ~Buycandie();
    
private slots:
    void on_buyCandieButton_clicked();

    void on_annullerButton_clicked();

    void on_pushButtonNextCandyType_clicked();

    void on_pushButtonNextAmount_clicked();

    void on_pushButtonAnuller_clicked();

    void on_pushButtonAnullerAmount_clicked();

    void on_pushButtonAnullerPayOrMore_clicked();

    void on_pushButtonChooseMore_clicked();
    
    void on_pushButtonPay_clicked();

    void on_pushButtonCancelOrder_clicked();

    void on_pushButtonPayOrder_clicked();

private:
    void checkPayment();

    Ui::Buycandie *ui;
    boost::shared_ptr<Ordermanager> orderMan_;
    boost::shared_ptr< CandyDB > m_db;
    bool bagPresent_;
    bool waitingForBag_;
    bool orderIsPacking_;
    int m_currentInsertedMoney;
};

#endif // BUYCANDIE_H
