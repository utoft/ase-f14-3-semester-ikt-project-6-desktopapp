#include "psocreader.h"
#include "GenericMessage.h"

#include <limits>

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <fstream>

#include <boost/scoped_ptr.hpp>

/**
 * @brief PSoCReader::PSoCReader Reads int's direcly from the kernel
 * @param readPath Path to read from
 * @param id The id to publish messages under
 * @param nonBlockStart True if the path initially should be read to get initial value without waiting
 */
PSoCReader::PSoCReader(std::string readPath, mediator::MessageIdentifier id, bool nonBlockStart)
    : ServiceLoaderModes(true), m_readPath(readPath), m_id(id), m_nonblock(nonBlockStart) {
}

/**
 * @brief PSoCReader::run Start reading from path. This need to be run in a thread
 * @param publisher Publisher to use to pubsish messages
 */
void PSoCReader::run(boost::shared_ptr<mediator::Publisher> publisher) {
    int fd = 0;
    size_t length = 0;
    char value[8] = {0};
    int intValue;

    std::cout << "Running " << this->m_readPath << std::endl;

    //If we need to initially read a value. Then do so
    if(m_nonblock) {
        fd = open(m_readPath.c_str(), O_RDONLY | O_NONBLOCK);

        if(fd < 0) {
            std::cout << "PSocCreator: NonBlock: Error: "<<  fd << " File not found " << m_readPath << std::endl;
            close(fd);
            return;
        }

        length = read(fd,value,sizeof(value));

        std::cout << "PSocCreator: NonBlock read from " << m_readPath << " length: " << length << " >>" << value << "<<" << std::endl;

        if(length != 4294967295)
        sendMessage(publisher,atoi(value));

        close(fd);
    } else
        std::cout << "PSocCreator: NonBlock read skipped for " << m_readPath << std::endl;


    //write(led,&ledvalue,sizeof(ledvalue));

    //C method
    fd = open(m_readPath.c_str(), O_RDONLY);

    if(fd < 0) {
        std::cout << "PSocCreator: Block: Error: "<<  fd << " File not found " << m_readPath << std::endl;
        close(fd);
        return;
    } else {
        std::cout << "PSocCreator: Block: Open: "<<  fd << m_readPath << std::endl;
    }

    for(;;) { //TODO: Find way to stop loop
        length = read(fd,value,sizeof(value));
        std::cout << "PSocCreator: Block read from " << m_readPath << " length: " << length << " >>" << value << "<<" << std::endl;

        if(length != 4294967295)
        sendMessage(publisher,atoi(value));
    }

    close(fd);

    /*
    //C++ Method
    std::ifstream infile;
    infile.open(m_readPath.c_str());

    if(!infile.is_open()) {
        std::cout << "PSocCreator: Block: Error: File not found " << m_readPath << std::endl;
        infile.close();
        return;
    } else {
        std::cout << "PSocCreator: Block: Open: " << m_readPath << std::endl;
    }

    for(;;) { //TODO: Find way to stop loop
        infile >> intValue;
        std::cout << "PSocCreator: Block read from " << m_readPath << " length: " << " >>" << intValue << "<<" << std::endl ;
        sendMessage(pub,intValue);
    }

    infile.close();*/
}

/**
 * @brief PSoCReader::sendMessage Sends a message about change in level
 * @param pub the publisher to use to send the information
 * @param num the number to send.
 */
void PSoCReader::sendMessage(boost::shared_ptr<mediator::Publisher> & pub, boost::int16_t num) {
    GenericMessage<int> * msgPtr = new GenericMessage<int>(num);
    boost::shared_ptr< GenericMessage<int> > message(msgPtr);
    pub->Publish(m_id,message);
}
